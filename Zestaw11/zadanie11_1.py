# -*- coding: utf-8 -*-
#
#Przygotować moduł Pythona z funkcjami tworzącymi listy liczb całkowitych do sortowania. Przydatne są m.in. następujące rodzaje danych:
#(a) różne liczby int od 0 do N-1 w kolejności losowej,
#(b) różne liczby int od 0 do N-1 prawie posortowane (liczby są blisko swojej prawidłowej pozycji),
#(c) różne liczby int od 0 do N-1 prawie posortowane w odwrotnej kolejności,
#(d) N liczb float w kolejności losowej o rozkładzie gaussowskim,
#(e) N liczb int w kolejności losowej, o wartościach powtarzających się, należących do zbioru k elementowego (k < N, np. k*k = N). 
import random, math

def randomIntList(N):
    """Funkcja zwraca listę zawierającą różne liczby int od 0 do N-1 w kolejności losowej."""
    l = list( range(N ) )
    random.shuffle(l )
    return l

def randomIntNearlySortedList(N):
    """Funkcja zwraca listę zawierającą różne liczby int od 0 do N-1 prawie posortowane."""
    l = list( range(N ) )
    difference = round(math.log(N)*2)
    
    for i in xrange(N):
        x = i - difference
        y = i + difference
        if x < 0:
            x = 0
        if y >= N:
            y = N - 1
        r = random.randint(x, y)
        l[i], l[r] = l[r], l[i]

    return l

def reverseRandomIntNearlySortedList(N):
    """Funkcja zwraca listę zawierającą różne liczby int od 0 do N-1 prawie posortowane w odwrotnej kolejności."""
    return randomIntNearlySortedList(N)[::-1]

def randomGaussList(N):
    """Funkcja zwracająca listę N liczb float w kolejności losowej o rozkładzie gaussowskim."""
    return list(random.gauss(N/2,N/6) for _ in xrange(N))

def randomReapetedList(N):
    """Funkcja zwracająca listę N liczb int w kolejności losowej, o wartościach powtarzających się,
     należących do zbioru k elementowego (k < N, np. k*k = N)."""
    return [random.randint(0,math.floor(math.sqrt(N))) for _ in xrange(N)]

#print randomIntList(8)

#print randomIntList(18)

#print randomIntNearlySortedList(8)

#print randomIntNearlySortedList(180)

#print reverseRandomIntNearlySortedList(8)

#print reverseRandomIntNearlySortedList(180)

#print randomGaussList(8)

#print randomGaussList(180)

#print randomReapetedList(8)

#print randomReapetedList(180)
