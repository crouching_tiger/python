# -*- coding: utf-8 -*-
#
#Poprawić wybrany algorytm sortowania, aby przyjmował jako dodatkowy argument funkcję porównującą elementy na liście
# (przykład na wykładzie dla bubblesort). 

import zadanie11_1

def shakersort(L, left, right,cmpfunc = cmp):
    k = right
    while left < right:
        for j in range(right, left, -1):   # od prawej
            if cmpfunc( L[j-1], L[j]):
                L[j-1], L[j] = L[j], L[j-1]
                k = j
        left = k
        for j in range(left, right):   # od lewej
            if cmpfunc( L[j], L[j+1]):
                L[j], L[j+1] = L[j+1], L[j]
                k = j
        right = k

def cmp2(x,y):
    if x < y:
        return 0
    else:
        return 1


L1 = zadanie11_1.randomIntList(8)
print L1

shakersort(L1, 0, 7,cmp2)
print L1


