# -*- coding: utf-8 -*-
#
import math
import os

class Castling(Exception):    #roszada - zakładamy że ruch w postaci "ruch króla ruch wiezy"
    """Exception to inform about possibility of castling."""
    def __init__(self, message, input_string):
        """Init castling exception."""
        self.message = message
        self.input_string = input_string

    def __str__(self):
        """Print exception message."""
        return self.message

class Move:
    """Set move attributes."""
    def __init__(self, start_ch, start_int, end_ch, end_int):
        self.start_x = ord( start_ch ) - ord('A')
        self.start_y = abs ( ord( start_int ) -ord('1')  - Board.size +1 )
        self.end_x = ord( end_ch ) - ord('A')
        self.end_y = abs( ord( end_int ) - ord('1') - Board.size +1)

    def __str__(self):
        """Print move."""
        return str(self.start_x) + str(self.start_y) + str(self.end_x) + str(self.end_y)

    def rook( self, board):
        """Check if move for rook is corect."""
        if self.start_x == self.end_x:    #the same column
            if self.start_y > self.end_y:    # go up
                a = self.start_y -1

                while a != self.end_y:    #search for obstacles on the way
                    if board.Array[self.start_x][a] != " ":
                        return False
                    a = a - 1

                return True
            else:   #go down
                a= self.start_y +1
                while a!= self.end_y:    #search for obstacles on the way
                    if board.Array[self.start_x][a] != " ":
                        return False
                    a= a + 1
                return True

        elif self.start_y == self.end_y:    #the same row
            if self.start_x > self.end_x:    # go left
                a = self.start_x -1
                while a != self.end_x:    #search for obstacles on the way
                    if board.Array[a][self.start_y] != " ":
                        return False
                    a = a - 1
                return True
            else:    #go right
                a = self.start_x + 1
                while a != self.end_x:    #search for obstacles on the way
                    if board.Array[a][self.start_y] != " ":
                        return False
                    a = a + 1
                return True
        else:    #incorrect move
            return False

    def knight( self, board):    # don't search for obstacles on the way - as in the chess rules
        """Check if move for knight is correct."""
        if abs( self.start_x - self.end_x ) == 2 and abs( self.start_y - self.end_y) == 1:    # 2 positions across and 1 up/down
            return True
        elif abs( self.start_x - self.end_x ) == 1 and abs( self.start_y - self.end_y) == 2:    # 2 positions up/down and 1 position across
            return True
        else:    #incorrect move
            return False

    def bishop( self, board):
        """Check if move for bishop is correct.""";
        if abs(self.start_x - self.end_x) == abs(self.start_y - self.end_y):    #begin and end position correct
            if self.start_x - self.end_x > 0:    # go left, search for obstacles on the way
                if self.start_y - self.end_y >0:    # go up
                    x = self.start_x - 1
                    y = self.start_y - 1
                    
                    while x != self.end_x:    # don't check y != self.end_y too, because they grow simultaneously
                        if board.Array[x][y] != " ":
                            return False
                        x = x - 1
                        y = y - 1
                    return True

                else:    #go down
                    x = self.start_x - 1
                    y = self.start_y + 1

                    while x != self.end_x:    # don't check y != self.end_y too, because they grow simultaneously
                        if board.Array[x][y] != " ":
                            print "uwaga"; raw_input();return False
                        x = x - 1
                        y = y + 1
                    return True

            else:    #go right
                if self.start_y - self.end_y >0:    # go up
                    x = self.start_x + 1
                    y = self.start_y - 1
                    
                    while x != self.end_x:    # don't check y != self.end_y too, because they grow simultaneously
                        if board.Array[x][y] != " ":
                            return False
                        x = x + 1
                        y = y - 1
                    return True

                else:    #go down
                    x = self.start_x + 1
                    y = self.start_y + 1

                    while x != self.end_x:    # don't check y != self.end_y too, because they grow simultaneously
                        if board.Array[x][y] != " ":
                            return False
                        x = x + 1
                        y = y + 1
                    return True

        else:    #incorrect move
            return False

    def king(self,board):
        """Check if move for king is correct."""
        if abs( self.start_x - self.end_x) == 1 or abs( self.start_y - self.end_y) == 1:
            return True    #we check only position, we don't search for obstacles, king goes just to next position next to start position 
        else:
            return False

    def pawn_black(self, board):
        """Check if move for black pawn is correct."""
        if self.start_y == 1 and self.start_y - self.end_y == -2:    #check if is in start line - pawn goes 2 positions
            if self.start_x == self.end_x:    #pawn go, can't attack another pawn
                if board.Array[self.start_x][self.start_y + 1] != ' ' or board.Array[self.start_x][self.start_y + 2] != ' ':
                    return False        #obstacle on the way
                else:
                    return True
            else:    #incorrect move
                return False
        elif self.start_y - self.end_y == -1:    #pawn goes normally, one position
            if self.start_x == self.end_x:    #pawn go
                if board.Array[self.start_x ][self.start_y + 1] != ' ':    #obstacle on the way
                    return False
                else:    #way is free
                    return True
            elif abs(self.start_x - self.end_x) == 1:    #pawn attack
                if board.Array[self.end_x ][self.end_y ] == ' ':    #pawn can't attack empty position
                    return False
                else:
                    return True
            else:    #incorrect move - not attack, not normal move
                return False    
        else:    #incorrect move
            return False
            

    def pawn_white(self, board):
        """Check if move for white pawn is correct."""
        if self.start_y == 6 and self.start_y - self.end_y == 2:    #check if is in start line - pawn goes 2 positions
            if self.start_x == self.end_x:    #correct move positions,pawn go, can't attack opponent
                if board.Array[self.start_x][self.start_y - 1] != ' ' or board.Array[self.start_x][self.start_y - 2] != ' ':
                    return False        #obstacle on the way
                else:    #there are no obstacles on the way
                    return True
            else:    #incorrect move
                return False
        elif self.start_y - self.end_y == 1:    #pawn goes normally, one position
            if self.start_x == self.end_x:    #pawn go
                if board.Array[self.start_x ][self.start_y - 1] != ' ':    #obstacle on the way
                    return False
                else:    #way is free
                    return True
            elif abs(self.start_x - self.end_x) == 1:    #pawn attack
                if board.Array[self.end_x ][self.end_y ] == ' ':    #pawn can't attack empty position
                    return False
                else:
                    return True
            else:    #incorrect move - not attack, not normal move
                return False    
        else:    #incorrect move
            return False
            
    def valid( self, board):
        """Check if move is valid for figure in start position."""
        figure = board.Array[self.start_x][self.start_y]

        if figure == u'♜' or figure == u'♖':
            return self.rook(board)

        elif figure == u'♞' or figure == u'♘':
            return self.knight(board)

        elif figure == u'♝' or figure == u'♗':
            return self.bishop(board)

        elif figure == u'♛' or  figure == u'♕':
            return self.rook(board) or self.bishop(board)

        elif figure == u'♚' or figure == u'♔':
            return self.king(board)

        elif figure == u'♟':
            return self.pawn_black(board)

        elif figure == u'♙':
            return self.pawn_white(board)


class Board:
    """Klasa odpowiadająca szachownicy."""
    size = 8   #rozmiar szachownicy

    def __init__(self):
        """Init board."""
        self.Array = [ [' ' for x in xrange(self.size)] for y in xrange(self.size)]

        self.Array[0][0]=u'♜'
        self.Array[1][0]=u'♞'
        self.Array[2][0]=u'♝'
        self.Array[3][0]=u'♛'
        self.Array[4][0]=u'♚'
        self.Array[5][0]=u'♝'
        self.Array[6][0]=u'♞'
        self.Array[7][0]=u'♜'

        self.Array[0][7]=u'♖'
        self.Array[1][7]=u'♘'
        self.Array[2][7]=u'♗'
        self.Array[3][7]=u'♕'
        self.Array[4][7]=u'♔'
        self.Array[5][7]=u'♗'
        self.Array[6][7]=u'♘'
        self.Array[7][7]=u'♖'

        for x in xrange( 8 ):
            self.Array[x][6]=u'♙'
            self.Array[x][1]=u'♟'

    def show(self):
        """Show current board."""
        os.system('clear')

        row = self.size

        print "     A   B   C   D   E   F   G   H"
        print "   +---+---+---+---+---+---+---+---+"

        for x in xrange(self.size):   #self.size, 0, -1
            line = " " + str(row) + " | "
            
            for y in xrange(self.size):
               line += self.Array[y][x] + " | "

            line += str(row)
            line += "\n   +---+---+---+---+---+---+---+---+"

            row = row -1
            print line

        print "     A   B   C   D   E   F   G   H"
            
    def getMove(self):
        """Get the move in form of string from keyboard."""
        while True:
            input_string = str(raw_input("Podaj współrzędne początkowe i końcowe ruchu: ") ).replace(' ', '')
        
            if self.correct_input(input_string) == True:
                return Move(input_string[0].upper(), input_string[1], input_string[2].upper(), input_string[3] )

            else:
                print "Incorret input format. Try again" #kasowanie linijek z ekranu i czekanie na nowy ruch
                raw_input("Press enter to continue.")
                print "\033[A\33[2K\r"
                print "\033[2A\33[2K"
                print "\033[2A\33[2K"
                print "\033[2A"

    def correct_input(self, input_string):
        """Check if input is in the correct form, eg. A2A3"""
        if len( input_string ) != 4:
            if len(input_string) == 8:    #Castling, roszada, wyjątek
                if self.correct_input( input_string[:4]) and self.correct_input( input_string[4:]):
                    raise Castling("pottential castling", input_string)
                else:
                    return False
            else:
                return False

        if not input_string[0].isalpha():
            return False

        if not input_string[1].isdigit():
            return False

        if not input_string[2].isalpha():
            return False

        if not input_string[3].isdigit():
            return False

        return True

    def makeMove(self, move):
        """Make move."""        
        self.Array[move.end_x][move.end_y] = self.Array[move.start_x][move.start_y]
        self.Array[move.start_x][move.start_y] = ' '

    def inBoard(self, move):
        """Check if move is in border of board."""
        if move.start_x >= 0 and move.start_x < Board.size:            #check if begin position across in Board
            if move.start_y >= 0 and move.start_y < Board.size:        #check if begin position down in Board
                if move.end_x >=0 and move.end_x < Board.size:         #check if end position across is in Bard
                    if move.end_y >= 0 and move.end_y < Board.size:    #check if end position down in Board
                        return True
        return False

    def startIsEmpty(self, move):
        """Check if start position contains any figure."""
        if self.Array[move.start_x][move.start_y] == " ":
            return True
        return False

    def startPositionIsEndPosition(self, move):
        """Check if start position of move is the same as end position of move."""
        if move.start_x == move.end_x and move.start_y == move.end_y:
            return True
        return False

    def sameColourOfFigures(self, move ):
        """Check if both of figures have the same colour, or we have figure and empty position."""

        figure1 = self.Array[move.start_x][move.start_y]
        figure2 = self.Array[move.end_x][move.end_y]

        if self.figureIsWhite(figure1) and self.figureIsWhite(figure2):   # two white figures
            return True

        if self.figureIsBlack(figure1)  and self.figureIsBlack(figure2):    #two figures are black
            return True

        return False    #figures have different colours or we have figure and empty space, or two empty space(another condition check it)

    def valid(self, move):
        """Check validation of move in board - if start of move is empty, start position of move is the same as end position of move,
            or if move is in border of board, or if start and end position of move have the same colour of figure."""
        correct = self.inBoard( move) and not self.startIsEmpty( move) and not self.startPositionIsEndPosition(move)
        correct = correct and not self.sameColourOfFigures(move)
        return correct

    def figureIsWhite( self, figure):
        """Check if figure is white."""
        if ord(figure) >= ord(u'♔') and ord(figure) <= ord(u'♙'):
            return True
        return False

    def figureIsBlack( self, figure):
        """Check if figure is black."""
        if ord(figure) >= ord(u'♚') and ord(figure) <= ord(u'♟'):
            return True
        return False

    def validColourMove(self, figure, white_move):
        """Check if we are trying to move our(white or black) figure."""
        if white_move:    #user white should make a move
            if self.figureIsWhite(figure):
                return True
            return False
        else:
            if self.figureIsBlack(figure):    #user black should make a move
                return True
            return False

    def Promotion(self, move):
        """Check if we need to make promotion of a pawn."""
        if self.Array[move.end_x][move.end_y] == u'♙':    #check if moved figure is a pawn
            if move.end_y == Board.size - Board.size:    #if pawn is now in first/last row of a board
                return True
            else:
                return False
        if self.Array[move.end_x][move.end_y] == u'♟':    #check if moved figure is a pawn
            if move.end_y == Board.size -1:    #if pawn is now in first/last row of a board
                return True
            else:
                return False
        return False    #there is no need to make promotion of a figure

    def makePromotion(self, move, white_move):
        """Make a promotion of a pawn."""

        print "\033[A\33[2K\r"
        print "\033[2A\33[2K"
        print "\033[2A\33[2K"
        print "\033[2A"

        print "Promocja figury. Wybierz numer nowej figury i wciśnij enter.:"
        print "1 - ", u'♜'
        print "2 - ", u'♞'
        print "3 - ", u'♝'
        print "4 - ", u'♛'

        choice = None
        
        while True:
            choice = int(raw_input("Wybierz na jaką figurę chcesz zmienić pionka: ") )

            if choice >= 1 and choice <= 4:
                break
            else:
                print "Niepoprawna liczba. Spróbuj ponownie."
                raw_input("Press enter to continue.")
                print "\033[A\33[2K\r"
                print "\033[2A\33[2K"
                print "\033[2A\33[2K"
                print "\033[2A"

        if white_move:    #promocja białych
            if choice == 1:
                self.Array[move.end_x][move.end_y] = u'♖'
            elif  choice == 2:
                self.Array[move.end_x][move.end_y] = u'♘'
            elif choice == 3:
                self.Array[move.end_x][move.end_y] = u'♗'
            elif choice == 4:
                self.Array[move.end_x][move.end_y] = u'♕'

    def castling(self, move1, move2, white_move, moved_kings, moved_left_rooks, moved_right_rooks):
        """Check if castling is correct."""
        if not (self.Array[move1.start_x][move1.start_y] == u'♚' or self.Array[move1.start_x][move1.start_y] == u'♔'): #ruch 1 - nie król
            if self.Array[move2.start_x][move2.start_y] == u'♚' or self.Array[move2.start_x][move2.start_y] == u'♔': # ruch drugi to król
                move2,move1 = move1, move2 #zamieniamy ruchy, żeby pierwszy był ruch króla
            else:
                return False    #pierwszy ruch, ani drugi ruch nie dotyczy króla

        if not (self.Array[move2.start_x][move2.start_y] == u'♜' or self.Array[move2.start_x][move2.start_y] == u'♖'):
            return False    #drugi ruch nie dotyczy króla

        if moved_kings[self.Array[move1.start_x][move1.start_y]] == True:
            return False    #to nie jest pierwszy ruch króla, to jednocześnie sprawdza czy są w pierwszejlinii, nieruszone = pierwsza linia

        if move2.start_x == 0:    #lewa wieża
            if moved_left_rooks[self.Array[move2.start_x][move2.start_y]] == True:
                return False    #to nie pierwszy ruch tej wieży
        elif move2.start_x == 7:
            if moved_right_rooks[self.Array[move2.start_x][move2.start_y]] == True:
                return False    #to nie pierwszy ruch tej wieży

        if not move1.start_y == move1.end_y:    #próbujemy zmienić linię
            return False

        if not move2.start_y == move2.end_y:    #próbujemy zmienić linię
            return False

        if move2.start_x == 0:   #szukamy przeszkód między lewą wieżą a królem
            i = move2.start_x +1
            while i < move1.start_x:
                if self.Array[i][move1.start_y] != " ":
                    return False
                i = i+1
        elif move2.start_x == 7:    #szukamy przeszkód między królem a prawą wieżą
            i = move1.start_x + 1
            while i < move2.start_x:
                if self.Array[i][move1.start_y] != " ":
                    return False
                i = i + 1

        if move2.start_x == 0:    #większa roszada
            if not move2.start_x +3 == move2.end_x:
                return False    #niepoprawny ruch wieży
            if not move1.start_x -2 == move1.end_x:
                return False    #niepoprawny ruch króla
        elif move2.start_x == 7:    #mniejsza roszada
            if not move2.start_x - 2 == move2.end_x:
                return False    #niepoprawny ruch króla
            if not move1.start_x + 2 == move1.end_x:
                return False    #niepoprawy ruch wieży

        return True    #ruch roszady jest poprawny

    def makeCastling(self, move1, move2):
        """Make castling."""
        self.makeMove( move1)
        self.makeMove( move2)

    def signCastling(self, move1, move2, moved_kings, moved_left_rooks, moved_right_rooks):
        """Sign variables as castling has been made."""
    #pozycje end ruchów, bo już zrobiliśmy ruch, więc pola start są teraz wolne
        if self.Array[move2.end_x][move2.end_y] == u'♚' or self.Array[move2.end_x][move2.end_y] == u'♔': # ruch drugi to król
            move2, move1 = move1, move2    #zmieniamy by król był w pierwszym ruchu

        figure1 = self.Array[move1.end_x][move1.end_y]
        figure2 = self.Array[move2.end_x][move2.end_y]

        moved_kings[figure1] = True

        if move2.start_x == 0:
            moved_left_rooks[figure2] = True
        else:
            moved_right_rooks[figure2] = True

class Game:
    """Class to describe game in chess."""

    def __init__(self):
        self.board = Board()
        self.white_move = True
        self.end_game = False
        self.winner_white = None
        #dla roszady - roszada gdy - król jeszcze się nie ruszył i wieża jeszcze się nie ruszyła, pomiędzy nimi nic nie ma, pola 
        #przez które przejdzie król nie są szachowane

        self.moved_kings = { u'♚':False, u'♔':False}
        self.moved_left_rooks = { u'♜':False, u'♖':False}
        self.moved_right_rooks = { u'♜':False, u'♖':False}

        self.board.show()
    
    def checkEnd(self, board, move):
        """Check if king is king is attacked."""
        if board.Array[move.end_x][move.end_y] == u'♚':
            self.end_game = True
            self.winer_white = False
        if board.Array[move.end_x][move.end_y] == u'♔':
            self.end_game = True
            self.winer_white = True

    def showEnd(self):
        """Show ending informations about game."""
        print "KONIEC GRY!!!"
        print "WYGRYWA GRACZ:"

        if self.winner_white:
            print "\tBIAŁY"
        else:
            print "\tCZARNY"

        print "GRATULACJE!!!!!!!!!"

    def play(self):
        """Play game in chess."""
        while( self.end_game == False ):

            if self.white_move:
                print "Ruch białych."
            else:
                print "Ruch czarnych."

            try:
                while True:                               #something like do getMove while( move is Incorrect)
                
                    move = self.board.getMove()

                    if  self.board.valid( move ) and move.valid(self.board) and self.board.validColourMove(self.board.Array[move.start_x][move.start_y],self.white_move):
                                       #if board.valid false (np. nie ma tam figury w polu startowym) to and nie sprawdza dalej, więc
                                       # w move.valid nie wystąpi sytuacja że pole puste, figura pusta, spoza zbioru figur szachowych
                        break 
                    else:    #kasowanie linijek z ekranu i czekanie na nowy ruch
                        print "Incorret move. Try again. Write move correct with chess rules." 
                        raw_input("Press enter to continue.")
                        print "\033[A\33[2K\r"
                        print "\033[2A\33[2K"
                        print "\033[2A\33[2K"
                        print "\033[2A"

                self.checkEnd(self.board, move);
                self.board.makeMove(move)
                if self.board.Promotion(move):    #check if game should make promotion of figure, pawn to another figure
                    self.board.makePromotion(move, self.white_move)

            except Castling, e:    #the same what "except Castling as e:"
                move1 = Move( e.input_string[0].upper(),e.input_string[1],e.input_string[2].upper(),e.input_string[3])
                move2 = Move( e.input_string[4].upper(), e.input_string[5], e.input_string[6].upper(), e.input_string[7])
                if self.board.valid( move1) and self.board.valid( move2):    #moves inside board
                    if self.board.validColourMove(self.board.Array[move1.start_x][move1.start_y], self.white_move):    #figure ownership
                        if self.board.validColourMove(self.board.Array[move2.start_x][move2.start_y], self.white_move): #as above
                            if self.board.castling(move1, move2, self.white_move, self.moved_kings, self.moved_left_rooks, self.moved_right_rooks):    #if Castling is correct
                                self.board.makeCastling(move1, move2)    #make Castling (zrób roszadę)

                                self.board.signCastling(move1, move2, self.moved_kings, self.moved_left_rooks, self.moved_right_rooks)
                                self.white_move = not self.white_move
                                self.board.show()

                print "Incorret move. Try again. Write move correct with chess rules." 
                raw_input("Press enter to continue.")
                print "\033[A\33[2K\r"
                print "\033[2A\33[2K"
                print "\033[2A\33[2K"
                print "\033[2A\33[2K"
                print "\033[2A"

            else:
                self.white_move = not self.white_move
                self.board.show()

        self.showEnd()

Game = Game()
Game.play()
