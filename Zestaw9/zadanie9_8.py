# -*- coding: iso-8859-2 -*-
#
#Dla drzewa BST napisać funkcje znajdujące największy i najmniejszy element przechowywany w drzewie.
# Mamy łącze do korzenia, nie ma klasy BinarySearchTree. Drzewo BST nie jest modyfikowane, a zwracana jest znaleziona wartość.
# W przypadku pustego drzewa należy wyzwolić wyjątek ValueError. 

class Node:
    """Klasa reprezentująca węzeł drzewa binarnego."""

    def __init__(self, data=None, left=None, right=None):
        self.data = data
        self.left = left
        self.right = right

    def __str__(self):
        return str(self.data)

    def insert(self, data):
        if self.data < data:      # na prawo
            if self.right:
                self.right.insert(data)
            else:
                self.right = Node(data)
        elif self.data > data:    # na lewo
            if self.left:
                self.left.insert(data)
            else:
                self.left = Node(data)
        else:
            pass    # ignoruję duplikaty

    def count(self):
        counter = 1
        if self.left:
            counter += self.left.count()
        if self.right:
            counter += self.right.count()
        return counter

    def search(self, data):
        if self.data == data:
            return True
        if data < self.data:
            if self.left:
                return self.left.search(data)
        else:
            if self.right:
                return self.right.search(data)
        return False

def bst_max(top):
    """Maksymalna wartośc w drzewie."""
    if not top:
        raise ValueError("Tree is empty.")

    current_element = top

    while current_element.right:
        current_element=current_element.right
    
    return current_element.data

def bst_min(top):
    """Minimalna wartość w drzewie."""
    if not top:
        raise ValueError("Tree is empty.")

    current_element = top

    while current_element.left:
        current_element = current_element.left

    return current_element.data

root = None
try:
    bst_max(root)
except ValueError:
    print "Wyjątek. Drzewo nie jest puste."

root = None
try:
    bst_min(root)
except ValueError:
    print "Wyjątek. Drzewo nie jest puste."

root = Node(14)
root.insert(1)
root.insert(2)
root.insert(-2)
root.insert(12)
root.insert(22)
root.insert(6)

print "Maksimum w drzewie: " + str( bst_max(root))
print "Minimum w drzewie: " + str( bst_min(root))
