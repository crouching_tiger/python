# -*- coding: iso-8859-2 -*-
#
#Mamy listy jednokierunkowe bez klasy SingleList.
# Napisać funkcję merge(), która łączy dwie listy przez podłączenie drugiej na koniec pierwszej, a zwraca nowy początek wspólnej listy.
# Uwzględnić możliwość pustych list.

class Node:
    """Klasa reprezentująca węzeł listy jednokierunkowej."""

    def __init__(self, data=None, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return str(self.data)   # bardzo ogólnie

def traverse(node, visit):
    """Iteracyjne przejście przez listę jednokierunkową."""
    while node:
        visit(node)
        node = node.next

def print_node(node):
    """Iteracyjne wypisanie listy jednokierunkowej."""
    print node.data,

def getLastNode(node):
    """Znajdź ostatni element listy."""
    while node.next:
        node = node.next

    return node

def merge(node1, node2):
    """Łącz listy."""
    if node1 != None and node2 != None:    #obie listy niepuste
        last1 = getLastNode(node1)
        last1.next = node2
        return node1

    elif node2 != None:    #pierwsza lista pusta, czyli druga lista niepusta
        node1 = node2
     #warunku gdy druga lista pusta nie sprawdzamy, bo wtedy i tak do pierwszej nic nie dołączamy
    return node1        

head1 = None #[]
head1 = Node(1, head1) # [1]
head1 = Node(3, head1) #[3,1]
head1 = Node(6, head1) #[6,3,1]
head1 = Node(8, head1) #[8,6,3,1]
head1 = Node(1, head1) #[1,8,6,3,1]
head1 = Node(6, head1) #[6,1,8,6,3,1]

head2 = None #[]
head2 = Node(-1, head2) # [-1]
head2 = Node(-3, head2) #[-3,-1]
head2 = Node(-6, head2) #[-6,-3,-1]
head2 = Node(-8, head2) #[-8,-6,-3,-1]
head2 = Node(-1, head2) #[-1,-8,-6,-3,-1]
head2 = Node(-6, head2) #[-6,-1,-8,-6,-3,-1]
head2 = Node(2, head2) #[2,-6,-1,-8,-6,-3,-1]
head2 = Node(3, head2) #[3,2,-6,-1,-8,-6,-3,-1]
head2 = Node(4, head2) #[4,3,2,-6,-1,-8,-6,-3,-1]

print "Lista 1"
traverse( head1, print_node)

print "\nLista 2"
traverse( head2, print_node)

# Zastosowanie.
head3 = merge(head1, head2)

# Czyszczenie łączy.
head1 = None                  # albo: del head1
head2 = None                  # albo: del head2

print "\nLista 3"
traverse( head3, print_node)




print "\nPuste listy:"
head4 = None #[]
head5 = None #[]

print "Pierwsza pusta lista:\n"
traverse(head4, print_node)

print "Druga pusta lista:\n"
traverse(head5, print_node)

head6 = merge(head4, head5)
print "Połączone puste listy:\n"
traverse(head6, print_node)
