#zadanie6_2.py - test

import unittest
from points import *

class TestPoint(unittest.TestCase):

    def setUp(self): pass

    def test_str(self):
        self.assertEqual( Point.__str__( Point(5,6) ), "(5,6)" )
        self.assertEqual( Point.__str__( Point(0,0) ), "(0,0)" )
        self.assertEqual( Point.__str__( Point(-1,1) ), "(-1,1)" )
        self.assertEqual( Point.__str__( Point(-5,6) ), "(-5,6)" )
        self.assertEqual( Point.__str__( Point(5,-6) ), "(5,-6)" )

    def test_repr(self):
        self.assertEqual( Point.__repr__( Point(5,6) ), "Point(5,6)" )
        self.assertEqual( Point.__repr__( Point(0,0) ), "Point(0,0)" )
        self.assertEqual( Point.__repr__( Point(-1,1) ), "Point(-1,-1)" )
        self.assertEqual( Point.__repr__( Point(-5,6) ), "Point(-5,6)" )
        self.assertEqual( Point.__repr__( Point(5,-6) ), "Point(5,-6)" )

    def test_eq(self):
        self.assertTrue( Point.__eq__( Point(0,0) , Point(0,0) ) )
        self.assertTrue( Point.__eq__( Point(1,1) , Point(1,1) ) )
        self.assertTrue( Point.__eq__( Point(11,11) , Point(11,11) ) )
        self.assertTrue( Point.__eq__( Point(10,10) , Point(10,10) ) )
        self.assertTrue( Point.__eq__( Point(-1,-1) , Point(-1,-1) ) )

        self.assertFalse( Point.__eq__( Point(0,0) , Point(10,0) ) )
        self.assertFalse( Point.__eq__( Point(1,-1) , Point(1,1) ) )
        self.assertFalse( Point.__eq__( Point(11,-11) , Point(11,11) ) )
        self.assertFalse( Point.__eq__( Point(10,10) , Point(10,-10) ) )
        self.assertFalse( Point.__eq__( Point(-1,-1) , Point(1,-1) ) )

    def test_ne(self):
        self.assertFalse( Point.__ne__( Point(0,0) , Point(0,0) ) )
        self.assertFalse( Point.__ne__( Point(1,1) , Point(1,1) ) )
        self.assertFalse( Point.__ne__( Point(11,11) , Point(11,11) ) )
        self.assertFalse( Point.__ne__( Point(10,10) , Point(10,10) ) )
        self.assertFalse( Point.__ne__( Point(-1,-1) , Point(-1,-1) ) )

        self.assertTrue( Point.__ne__( Point(0,0) , Point(10,0) ) )
        self.assertTrue( Point.__ne__( Point(1,-1) , Point(1,1) ) )
        self.assertTrue( Point.__ne__( Point(11,-11) , Point(11,11) ) )
        self.assertTrue( Point.__ne__( Point(10,10) , Point(10,-10) ) )
        self.assertTrue( Point.__ne__( Point(-1,-1) , Point(1,-1) ) )

    def test_add(self):
        self.assertEqual( Point.__add__( Point(0,0) , Point(0,0) ), Point(0,0) )
        self.assertEqual( Point.__add__( Point(1,1) , Point(1,1) ), Point(2,2) )
        self.assertEqual( Point.__add__( Point(-1,0) , Point(1,1) ), Point(0,1) )
        self.assertEqual( Point.__add__( Point(1,-1) , Point(1,1) ), Point(2,0) )
        self.assertEqual( Point.__add__( Point(2,2) , Point(-1,-1) ), Point(1,1) )
        self.assertEqual( Point.__add__( Point(11,11) , Point(1,1) ), Point(12,12) )
        self.assertEqual( Point.__add__( Point(-1,-1) , Point(-1,-1) ), Point(-2,-2) )

    def test_sub(self):
        self.assertEqual( Point.__sub__( Point(0,0) , Point(0,0) ), Point(0,0) )
        self.assertEqual( Point.__sub__( Point(1,1) , Point(1,1) ), Point(0,0) )
        self.assertEqual( Point.__sub__( Point(-1,0) , Point(1,1) ), Point(-2,-1) )
        self.assertEqual( Point.__sub__( Point(1,-1) , Point(1,1) ), Point(0,-2) )
        self.assertEqual( Point.__sub__( Point(2,2) , Point(-1,-1) ), Point(3,3) )
        self.assertEqual( Point.__sub__( Point(11,11) , Point(1,1) ), Point(10,10) )
        self.assertEqual( Point.__sub__( Point(-1,-1) , Point(-1,-1) ), Point(0,0) )

    def test_mul(self):
        self.assertEqual( Point.__mul__( Point(0,0) , Point(0,0) ), 0 )
        self.assertEqual( Point.__mul__( Point(1,1) , Point(1,1) ), 2 )
        self.assertEqual( Point.__mul__( Point(-1,0) , Point(1,1) ), -1 )
        self.assertEqual( Point.__mul__( Point(1,-1) , Point(1,1) ), 0 )
        self.assertEqual( Point.__mul__( Point(2,2) , Point(-1,-1) ), -4 )
        self.assertEqual( Point.__mul__( Point(11,11) , Point(1,1) ), 22 )
        self.assertEqual( Point.__mul__( Point(-1,-1) , Point(-1,-1) ), 2 )

    def test_cross(self):
        self.assertEqual( Point.cross( Point(0,0) , Point(0,0) ), 0 )
        self.assertEqual( Point.cross( Point(1,1) , Point(1,1) ), 0 )
        self.assertEqual( Point.cross( Point(-1,0) , Point(1,1) ), -1 )
        self.assertEqual( Point.cross( Point(1,-1) , Point(1,1) ), 2 )
        self.assertEqual( Point.cross( Point(2,2) , Point(-1,-1) ), 0 )
        self.assertEqual( Point.cross( Point(11,11) , Point(1,1) ), 0 )
        self.assertEqual( Point.cross( Point(-1,-1) , Point(-1,-1) ), 0 )

    def test_length(self):
        self.assertEqual( Point.length( Point(0,0) ), 0 )
        self.assertEqual( Point.length( Point(3,4) ), 5 )
        self.assertEqual( Point.length( Point(-3,-4) ), 5 )
        self.assertEqual( Point.length( Point(9,12) ), 15 )
        self.assertEqual( Point.length( Point(3,-4) ), 5 )
        self.assertEqual( Point.length( Point(-3,4) ), 5 )
        self.assertEqual( Point.length( Point(5,12) ), 13 )

    def tearDown(self): pass

if __name__ == '__main__':
    unittest.main()     # uruchamia wszystkie testy

