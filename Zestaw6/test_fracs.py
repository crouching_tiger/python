# -*- coding: iso-8859-2 -*-
#
#zadanie6_5.py

import unittest
from fracs import *

class TestFrac(unittest.TestCase):

    def setUp(self): pass

    def test_str(self):
        self.assertEqual( Frac.__str__( Frac(5,6) ), "5/6" )
        self.assertEqual( Frac.__str__( Frac(0,1) ), "0" )
        self.assertEqual( Frac.__str__( Frac(-1,-1) ), "-1/-1" )
        self.assertEqual( Frac.__str__( Frac(-5,6) ), "-5/6" )
        self.assertEqual( Frac.__str__( Frac(5,-6) ), "5/-6" )

    def test_repr(self):
        self.assertEqual( Frac.__repr__( Frac(5,6) ), "Frac(5, 6)" )
        self.assertEqual( Frac.__repr__( Frac(0,1) ), "Frac(0, 1)" )
        self.assertEqual( Frac.__repr__( Frac(-1,-1) ), "Frac(-1, -1)" )
        self.assertEqual( Frac.__repr__( Frac(-5,6) ), "Frac(-5, 6)" )
        self.assertEqual( Frac.__repr__( Frac(5,-6) ), "Frac(5, -6)" )

    def test_add(self):
        self.assertEqual(Frac(1, 2) + Frac(1, 3), Frac(5, 6) )    # >0 , >0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) + Frac(1, 3), Frac(-1, 6) )  # <0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) + Frac(-1, 3), Frac(1, 6) )   # >0 , <0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) + Frac(-1, 3), Frac(-5, 6) ) # <0 , <0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) + Frac(1, 2), Frac(0, 1) )   # <0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) + Frac(-1, 2), Frac(0, 1) )   # >0 , <0 l.niecałkowita

        self.assertEqual(Frac(-11, 2) + Frac(111, 3), Frac(63, 2) ) # <0 , >0 l.niecałkowita

        self.assertEqual(Frac(1, 1) + Frac(1, 1), Frac(2, 1) )    # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1, 1) + Frac(-1, 1), Frac(-2, 1) ) # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1, 1) + Frac(1, 1), Frac(0, 1) )   # >0 , >0 l.całkowita
        self.assertEqual(Frac(1, 1) + Frac(-1, 1), Frac(0, 1) )   # >0 , >0 l.całkowita

        self.assertEqual(Frac(0, 1) + Frac(0, 1), Frac(0, 1) ) # ==0 , ==0

    def test_sub(self):
        self.assertEqual(Frac(1, 2) - Frac(1, 3), Frac(1, 6) )    # >0 , >0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) - Frac(1, 3), Frac(-5, 6) )  # <0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) - Frac(-1, 3), Frac(5, 6) )   # >0 , <0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) - Frac(-1, 3), Frac(-1, 6) ) # <0 , <0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) - Frac(1, 2), Frac(-1, 1) )  # <0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) - Frac(-1, 2), Frac(1, 1) )   # >0 , <0 l.niecałkowita
        self.assertEqual(Frac(1, 2) - Frac(1, 2), Frac(0, 1) )    # >0 , >0 l.niecałkowita

        self.assertEqual(Frac(1, 1) - Frac(1, 1), Frac(0, 1) )   # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1, 1) - Frac(-1, 1), Frac(0, 1) ) # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1, 1) - Frac(1, 1), Frac(-2, 1) ) # >0 , >0 l.całkowita
        self.assertEqual(Frac(1, 1) -Frac(-1, 1), Frac(2, 1) )   # >0 , >0 l.całkowita

        self.assertEqual(Frac(0, 1) - Frac(0, 1), Frac(0, 1) ) # ==0 , ==0

    def test_mul(self):
        self.assertEqual(Frac(1, 2) * Frac(1, 3), Frac(1, 6) )   # >0 , >0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) * Frac(1, 3), Frac(-1, 6) ) # <0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) * Frac(-1, 3), Frac(-1, 6) ) # >0 , <0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) * Frac(-1, 3), Frac(1, 6) ) # <0 , <0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) * Frac(1, 2), Frac(-1, 4) ) # >0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) * Frac(-1, 2), Frac(-1, 4) ) # >0 , <0 l.niecałkowita
        self.assertEqual(Frac(1, 2) * Frac(1, 2), Frac(1, 4) )   # >0 , >0 l.niecałkowita

        self.assertEqual(Frac(0, 2) * Frac(1, 2), Frac(0, 1) ) # ==0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) * Frac(0, 2), Frac(0, 1) ) # >0 , ==0 l.niecałkowita
        self.assertEqual(Frac(0, 2) * Frac(0, 2), Frac(0, 1) ) # ==0 , ==0 l.niecałkowita

        self.assertEqual(Frac(1, 1) * Frac(1, 1), Frac(1, 1) )    # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1, 1) * Frac(-1, 1), Frac(1, 1) )  # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1, 1) * Frac(1, 1), Frac(-1, 1) )  # >0 , >0 l.całkowita
        self.assertEqual(Frac(1, 1) * Frac(-1, 1), Frac(-1, 1) )  # >0 , >0 l.całkowita

    def test_div(self):
        self.assertEqual(Frac(1, 2) / Frac(1, 3), Frac(3, 2) )   # >0 , >0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) / Frac(1, 3), Frac(-3, 2) ) # <0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) / Frac(-1, 3), Frac(-3, 2) ) # >0 , <0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) / Frac(-1, 3), Frac(3, 2) ) # <0 , <0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) / Frac(1, 2), Frac(-1, 1) ) # <0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) / Frac(-1, 2), Frac(-1, 1) ) # >0 , <0 l.niecałkowita
        self.assertEqual(Frac(1, 2)/ Frac(1, 2), Frac(1, 1) )    # >0 , >0 l.niecałkowita

        self.assertEqual(Frac(0, 2) / Frac(1, 2), Frac(0, 1) )   # ==0, >0, l.niecałkowita

        self.assertEqual(Frac(1, 1) / Frac(1, 1), Frac(1, 1) )    # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1, 1) / Frac(-1, 1), Frac(1, 1) )  # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1, 1) / Frac(1, 1), Frac(-1, 1) )  # >0 , >0 l.całkowita
        self.assertEqual(Frac(1, 1) / Frac(-1, 1), Frac(-1, 1) )  # >0 , >0 l.całkowita

    def test_pos(self):
        self.assertEqual( +Frac(0,1) , Frac(0,1) ) # +0 == 0
        self.assertEqual( +Frac(1,1) , Frac(1,1) ) # 1 
        self.assertEqual( +Frac(-1,1) , Frac(-1,1) ) # -1

    def test_neq(self):
        self.assertEqual( -Frac(0,1) , Frac(0,1) ) # -0 == 0
        self.assertEqual( -Frac(1,1) , Frac(-1,1) ) # -1 
        self.assertEqual( -Frac(-1,1) , Frac(1,1) ) # 1

    def test_invert(self):
        self.assertEqual( ~Frac(1,1), Frac(1,1) ) 
        self.assertEqual( ~Frac(1,3), Frac(3,1) ) 
        self.assertEqual( ~Frac(-1,3), Frac(-3,1) ) 
        self.assertEqual( ~Frac(1,6), Frac(6,1) ) 

    def test_cmp(self):
        self.assertEqual( Frac.__cmp__( Frac(0,1), Frac(0,1) ), 0 )
        self.assertEqual( Frac.__cmp__( Frac(1,1), Frac(1,1) ), 0 )
        self.assertEqual( Frac.__cmp__( Frac(-1,1), Frac(-1,1) ), 0 )

        self.assertEqual( Frac.__cmp__( Frac(1,1), Frac(0,1) ), 1 )
        self.assertEqual( Frac.__cmp__( Frac(0,1), Frac(1,1) ), -1 )

    def test_float(self):
        self.assertEqual(Frac.__float__( Frac(1, 1) ), 1.0) # >0 l. całkowita 
        self.assertEqual(Frac.__float__( Frac(-1, 1) ), -1.0) # <0 l. całkowita 
        self.assertEqual(Frac.__float__( Frac(0, 1) ), 0.0)  # == 0

        self.assertEqual(Frac.__float__(Frac(1, 2) ), 0.5)  # >0 l. niecałkowita 
        self.assertEqual(Frac.__float__(Frac(1, 4) ), 0.25) # >0 l. niecałkowita 
        self.assertEqual(Frac.__float__(Frac(1, 10) ), 0.1) # >0 l. niecałkowita 

        self.assertEqual(Frac.__float__(Frac(-1, 2) ), -0.5)  # <0 l. niecałkowita 
        self.assertEqual(Frac.__float__(Frac(-1, 4) ), -0.25)  # <0 l. niecałkowita 
        self.assertEqual(Frac.__float__(Frac(-1, 10) ), -0.1)  # <0 l. niecałkowita 

    def test_lt(self):
        self.assertTrue( Frac(1,2) < Frac(2,1) )
        self.assertTrue( Frac(-8,2) < Frac(-2,1) )
        self.assertTrue( Frac(-1,2) < Frac(2,1) )

        self.assertFalse( Frac(1,2) < Frac(-2,1) )
        self.assertFalse( Frac(-1,2) < Frac(-2,1) )
        self.assertFalse( Frac(8,2) < Frac(2,1) )

    def test_le(self):
        self.assertTrue( Frac(1,2) <= Frac(1,2) )
        self.assertTrue( Frac(-1,2) <= Frac(-1,2) )
        self.assertTrue( Frac(0,1) <= Frac(0,1) )

        self.assertTrue( Frac(1,2) < Frac(2,1) )
        self.assertTrue( Frac(-8,2) < Frac(-2,1) )
        self.assertTrue( Frac(-1,2) < Frac(2,1) )

        self.assertFalse( Frac(1,2) < Frac(-2,1) )
        self.assertFalse( Frac(-1,2) < Frac(-2,1) )
        self.assertFalse( Frac(8,2) < Frac(2,1) )

    def test_gt(self):
        self.assertTrue( Frac(2,1) > Frac(1,2) )
        self.assertTrue( Frac(-2,1) > Frac(-8,2) )
        self.assertTrue( Frac(2,1) > Frac(-1,2) )

        self.assertFalse( Frac(-2,1) > Frac(1,2) )
        self.assertFalse( Frac(-2,1) > Frac(-1,2) )
        self.assertFalse( Frac(2,1) > Frac(8,2) )

    def test_ge(self):
        self.assertTrue( Frac(2,1) >= Frac(2,1) )
        self.assertTrue( Frac(-2,1) >= Frac(-2,1) )
        self.assertTrue( Frac(0,1) >= Frac(0,1) )

        self.assertTrue( Frac(2,1) >= Frac(1,2) )
        self.assertTrue( Frac(-2,1) >= Frac(-8,2) )
        self.assertTrue( Frac(2,1) >= Frac(-1,2) )

        self.assertFalse( Frac(-2,1) >= Frac(1,2) )
        self.assertFalse( Frac(-2,1) >= Frac(-1,2) )
        self.assertFalse( Frac(2,1) >= Frac(8,2) )

    def tearDown(self): pass

if __name__ == '__main__':
    unittest.main()     # uruchamia wszystkie testy

