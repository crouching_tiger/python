# -*- coding: iso-8859-2 -*-
#
#W pliku fracs.py zdefiniować klasę Frac wraz z potrzebnymi metodami.
#Ułamek jest reprezentowany przez parę liczb całkowitych. Napisać kod testujący moduł fracs.
#
#zadanie6_5.py

from fractions import gcd

class Frac:
    """Klasa reprezentująca ułamek."""

    def __init__(self, x=0, y=1):
        self.x = x
        self.y = y

    def __str__(self):         # zwraca "x/y" lub "x" dla y=1
        """wypisz "x/y" lub "x" dla y=1"""
        if self.y == 1:
            return "%s" % self.x
        else:
            return "%s/%s" % (self.x, self.y)

    def __repr__(self):        # zwraca "Frac(x, y)"
        """Wypisz "Frac(x,y)" """
        return "Frac(%s, %s)" % (self.x, self.y)

    def __add__(self, other):  # frac1 + frac2
        """Dodaj dwa ułamki"""
        mianownik = self.y * other.y
        frac = [ self.x * mianownik / self.y + other.x * mianownik / other.y , mianownik]
        divider = gcd(frac[0], frac[1])
        return Frac( frac[0] / divider, frac[1] / divider )


    def __sub__(self, other):  # frac1 - frac2
        """Odejmij dwa ułamki."""
        return self+(-other)

    def __mul__(self, other):  # frac1 * frac2
        """Pomnóż dwa ułamki"""
        frac = [ self.x * other.x, self.y * other.y ]
        divider = gcd( frac[0], frac[1] )
        return Frac( frac[0] / divider, frac[1] / divider )


    def __div__(self, other):  # frac1 / frac2
        """Podziel dwa ułamki"""
        return self * ~ other


    # operatory jednoargumentowe
    def __pos__(self):  # +frac = (+1)*frac
        """(+1)*Frac"""
        return self

    def __neg__(self):  # -frac = (-1)*frac
        """(-1)*Frac"""
        return Frac(-self.x, self.y)

    def __invert__(self):  # odwrotnosc: ~frac
        """Odwrotność ułamka"""
        return Frac(self.y, self.x)

    def __cmp__(self, other):  # cmp(frac1, frac2)
        """Porównaj dwa ułamki"""
        if self > other:
            return 1
        elif self < other:
            return -1
        else:
            return 0

    def __float__(self):       # float(frac)
        """Konwersja ułamka do postaci rzeczywistej"""
        return float(self.x)/float(self.y)

    def __lt__(self,other): # <
        """mniejszy niż inny ułamek."""
        frac = self - other
        if frac.x * frac.y < 0:
            return True
        else:
            return False

    def __le__(self,other): # <=
        """mniejszy lub równy niż inny ułamek."""
        frac = self - other
        if frac.x * frac.y <= 0:
            return True
        else:
            return False

    def __gt__(self,other): # >
        """większy niż inny ułamek."""
        frac = self - other
        if frac.x * frac.y > 0:
            return True
        else:
            return False

    def __ge__(self,other): # >=
        """większy lub równy niż inny ułamek."""
        frac = self - other
        if frac.x * frac.y >= 0:
            return True
        else:
            return False

