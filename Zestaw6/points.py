# -*- coding: iso-8859-2 -*-
#
#W pliku points.py zdefiniować klasę Point wraz z potrzebnymi metodami.
#Punkty są traktowane jak wektory zaczepione w początku układu współrzędnych, o końcu w położeniu (x, y).
#Napisać kod testujący moduł points.
#zadanie6_2.py

from math import sqrt

class Point:
    """Klasa reprezentująca punkty na płaszczyźnie."""

    def __init__(self, x=0, y=0):  # konstuktor
        self.x = x
        self.y = y

    def __str__(self):         # zwraca string "(x, y)"
        """Wypisz punkt "(x,y)"."""
        return "(%s,%s)" % (self.x, self.y)

    def __repr__(self):        # zwraca string "Point(x, y)"
        """Wypisz punkt "Point(x,y)"."""
        return "Point(%s,%s)" % (self.x, self.y)

    def __eq__(self, other):   # obsługa point1 == point2
        """Porównaj punkty."""
        return (self.x == other.x) and (self.y == other.y)

    def __ne__(self, other):        # obsługa point1 != point2
        return not self == other

    # Punkty jako wektory 2D.
    def __add__(self, other):  # v1 + v2
        """Suma wektorów."""
        return Point( self.x + other.x , self.y + other.y)

    def __sub__(self, other):  # v1 - v2
        """Różnica wektorów"""
        return Point( self.x - other.x , self.y - other.y)

    def __mul__(self, other):  # v1 * v2, iloczyn skalarny
        """Iloczyn skalarny wektorów"""
        return self.x * other.x + self.y * other.y 

    def cross(self, other):         # v1 x v2, iloczyn wektorowy 2D
        """Iloczyn wektorowy"""
        return self.x * other.y - self.y * other.x

    def length(self):          # długość wektora
        """Długość wektora"""
        return sqrt( self.x * self.x + self.y * self.y)


