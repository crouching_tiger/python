# -*- coding: iso-8859-2 -*-
#
#Mamy daną listę sekwencji (listy lub krotki) różnej długości zawierających liczby.
#Znaleźć listę zawierającą sumy liczb z tych sekwencji.
#Przykładowa sekwencja [[],[4],(1,2),[3,4],(5,6,7)], spodziewany wynik [0,4,3,7,18]. 

Lista = [ [],[4],(1,2),(5,6,7),[0,1,2,3,9,11],(),(11,14,3,12),(11,12),[-1,-2,-3,-6,111],(1,)]

print map(sum, Lista)
