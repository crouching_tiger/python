# -*- coding: iso-8859-2 -*-
#
#Stworzyć słownik tłumaczący liczby zapisane w systemie rzymskim (z literami I, V, X, L, C, D, M) na liczby arabskie 
#(podać kilka sposobów tworzenia takiego słownika).
# Mile widziany kod tłumaczący całą liczbę [funkcja roman2int()]. 
#
#metoda 1

D = { "I":1, "V":5, "X":10, "L":50, "C":100, "D":500, "M":1000}

#metoda 2
D2 = dict( I=1, V=5, X=10, L=50, C=100, D=500, M=1000)

#metoda 3
D3 = dict(zip(["I", "V", "X", "L", "C", "D", "M"], [1, 5, 10, 50, 100, 500, 1000]))

#metoda 4
D4 = dict([("I",1), ("V", 5), ("X",10), ("L",50), ("C",100), ("D", 500), ("M",1000)])

#metoda 5
D5 = {}

D5["I"] = 1
D5["V"] = 5
D5["X"] = 10
D5["L"] = 50
D5["C"] = 100
D5["D"] = 500
D5["M"] = 1000

#metoda 6
D6 = dict()

D6["I"] = 1
D6["V"] = 5
D6["X"] = 10
D6["L"] = 50
D6["C"] = 100
D6["D"] = 500
D6["M"] = 1000

#metoda 7
D7 = {}

D7.update({ "I":1, "V":5, "X":10, "L":50, "C":100, "D":500, "M":1000})

#metoda 8
D8 = dict()

D8.update({ "I":1, "V":5, "X":10, "L":50, "C":100, "D":500, "M":1000})

print D["I"]
print D2["I"]
print D3["I"]
print D4["I"]
print D5["I"]
print D6["I"]
print D7["I"]
print D8["I"]

def roman2int( roman ):
    result = 0
    i = 0

    while i < len( roman ):
        value1 = D[ roman[i] ]
        
        if i+1 < len( roman ):
            value2 = D[ roman[i+1] ]
            
            if value1 >= value2:
                result += value1
                i = i + 1
            else:
                result += value2
                result -= value1
                i = i + 2
        else:
            result += value1
            i = i + 1
	print result
    return result

print roman2int("MMCMXLV")

