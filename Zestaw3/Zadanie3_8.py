# -*- coding: iso-8859-2 -*-
#
#Dla dwóch sekwencji znaleźć: (a) listę elementów występujących w obu sekwencjach (bez powtórzeń),
# (b) listę wszystkich elementów z obu sekwencji (bez powtórzeń).
#
sekwencja1= (1, 2, 4, 4, 6, 7, 8, 9, 11, 22, 33, 44, 26, 26)
sekwencja2= (1, 8, 9, 26 , 44, 11, 38, 21, 12, 22, 1, 6)

s1 = set(sekwencja1)
s2 = set(sekwencja2)

print "Wspólne elementy "
print list( s1.intersection( s2 ) )
print "Elementy z obu sekwencji "
print list( s1.union( s2 ) )
