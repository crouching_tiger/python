# -*- coding: iso-8859-2 -*-
#
#Napisać program rysujący prostokąt zbudowany z małych kratek.
#Należy zbudować pełny string, a potem go wypisać. Przykładowy prostokąt składający się 2x4 pól ma postać:
#
#+---+---+---+---+
#|   |   |   |   |
#+---+---+---+---+
#|   |   |   |   | 
#+---+---+---+---+
#

try:
    Y = int(raw_input("Podaj liczbe kratek w pionie: "))
except ValueError:
    print "Niepoprawna wartość na wejściu. Error."  
    sys.exit

try:
    X = int(raw_input("Podaj liczbę kratek w poziomie: "))
except ValueError:
    print "Niepoprawna wartość na wejściu. Error."  
    sys.exit

new_output = ""

for i in xrange(Y):
    for j in xrange(X):
        new_output += "+---"
    new_output += "+\n"
    for j in xrange(X):
        new_output += "|   "
    new_output += "|\n"

for j in xrange(X):
    new_output += "+---"
new_output += "+\n"

  
print new_output
