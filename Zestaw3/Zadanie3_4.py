# -*- coding: iso-8859-2 -*-
#
#Napisać program pobierający w pętli od użytkownika liczbę rzeczywistą x i wypisujący parę x i trzecią potęgę x.
#Zatrzymanie programu następuje po wpisaniu z klawiatury stop.
#Jeżeli użytkownik wpisze napis zamiast liczby, to program ma wypisać komunikat o błędzie i kontynuować pracę.
#
X=raw_input("Podaj liczbę: ")

while(X != "stop"):
    try:
        X3 = pow(float(X),3)
        print X, X3

    except ValueError:
        print "To nie liczba. Error."    
    
    X = raw_input("Podaj liczbę: ")
