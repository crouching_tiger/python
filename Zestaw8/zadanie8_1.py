# -*- coding: iso-8859-2 -*-
#Zbadać problem szukania rozwiązań równania liniowego postaci a * x + b * y + c = 0.
# Podać specyfikację problemu. Podać algorytm rozwiązania w postaci listy kroków, schematu blokowego, drzewa.
# Podać implementację algorytmu w Pythonie w postaci funkcji solve1(), która rozwiązania wypisuje w formie komunikatów. 

def solve1( a, b, c):
    """Rozwiązywanie równania liniowego a x + b y + c = 0."""
    if a == 0:
        if b == 0:
            if c == 0:
                print "równanie nieokreslone. nieskonczenie wiele rozwiazan w zbiorze liczb rzeczywistych, x,y należących do R" # a=0 b=0 c=0
            else:
                print "rownanie jest sprzeczne"                                                               #a=0 b=0 c!=0
        else:
            print "rozwiązanie równania -prosta: x należące do R i  y = " + str( (-float(c) ) / float(b) )       #a=0 b!=0    y = -c/b
    else:
        if b == 0:                       
            print "rozwiązanie równania -prosta: y należące do R i x= " +  str(-float(c) / float(a) )            #a!=0 i b = 0
        else:                              #a!=0 b !=0  rozwiązanie:prosta y = (-c-a*x)/b, czyli zbiór punktów (y, (-c-a*x)/b)
            rownanie = "( "+ str(float(-c)) + " - " + str(float(a)) + "*x" +") / " + str( float(b) )
            print "rozwiązanie równania - prosta: y = " + rownanie 


solve1( 1, 1, 1)
solve1( 1, 2, 3)
