# -*- coding: iso-8859-2 -*-
#
#Za pomocą techniki programowania dynamicznego napisać program obliczający wartości funkcji P(i, j).
# Porównać z wersją rekurencyjną programu.
# Wskazówka: Wykorzystać tablicę dwuwymiarową (np. słownik) do przechowywania wartości funkcji.
# Wartości w tablicy wypełniać kolejno wierszami.
#
#P(0, 0) = 0.5,
#P(i, 0) = 0.0 dla i > 0,
#P(0, j) = 1.0 dla j > 0,
#P(i, j) = 0.5 * (P(i-1, j) + P(i, j-1)) dla i > 0, j > 0.

import time

#wersja rekurencyjna
def P(i, j):
    if i < 0 or j < 0:
        raise ValueError("Funkcja określona tylko dla argumentów większych lub równych 0, argumenty muszą być całkowite..")
    elif i == 0 and j == 0:
        return 0.5
    elif j == 0:
        return 0.0
    elif i == 0:
        return 1.0
    else:
        return 0.5 * (P(i-1, j) + P(i, j-1))
 
def P1(i, j):
    if i < 0 or j < 0:
        raise ValueError("Funkcja określona tylko dla argumentów większych lub równych 0.")
    elif i == 0 and j == 0:
        return 0.5
    elif j == 0:
        return 0.0
    elif i == 0:
        return 1.0
    else:
        value = P1.dictionary.get((i,j))
        if(value != None):
            return value
        else:
            value = 0.5*(P1(i-1, j) + P1(i, j-1))
            P1.dictionary[ (i, j) ] = value
            return value
P1.dictionary = {(0,0): 0.5, (0,1): 1.0, (1,0): 0.0}

def Pcompare(i, j):
    start = time.clock()
    Pvalue = str( P( i, j) )
    t = time.clock() - start

    start = time.clock()
    P1value = str( P1( i, j) )
    t1 = time.clock() - start
    
    print "P( " + str(i) + ", " + str(j) + ") = " + str(Pvalue) + " , czas = " + str(t) + "s"
    print "P1( " + str(i) + ", " + str(j) + ") = " + str(P1value) + " , czas = " + str(t1) + "s"

print P(3,3)      
print P1(3,3)  
Pcompare(12,14)
Pcompare(3,3)
