# -*- coding: iso-8859-2 -*-
#
#Zaimplementować algorytm obliczający pole powierzchni trójkąta, jeżeli dane są trzy liczby będące długościami jego boków.
# Jeżeli podane liczby nie spełniają warunku trójkąta, to program ma generować wyjątek ValueError.

import math

def heron( a, b, c):
    """Obliczanie pola powierzchni trójkąta za pomocą wzoru
    Herona. Długości boków trójkąta wynoszą a, b, c."""

    if  a + b <= c or a + c <= b or b + c <= a:
        raise ValueEror("niespełniony warunek trójkąta")
    else:
        p = 0.5 * ( a + b + c )
        return math.sqrt( p * (p - a ) * ( p - b ) * ( p - c ) ) 

try:
    print heron( 1, 1, 1)
    print heron( 3, 4 , 5)
except ValueError:
    print "pamiętaj o warunku trójkąta."

