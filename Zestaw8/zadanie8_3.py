# -*- coding: iso-8859-2 -*-
#
#Obliczyć liczbę pi za pomocą algorytmu Monte Carlo. Wykorzystać losowanie punktów z kwadratu z wpisanym kołem. Sprawdzić zależność dokładności wyniku od liczby losowań. Wskazówka: Skorzystać z modułu random.


import random

def calc_pi(n=100):
    """Obliczanie liczby pi metodą Monte Carlo.
    n oznacza liczbę losowanych punktów."""

    if n <= 0:
        raise ValueError("Liczba losowań nie może być mniejsza bądż równa 0.")

    w_kole = 0
    i = 0

    while( i < n ):
        liczba1 = (random.randint(0,10**9)*1.0 /10**9)        # losowanie z zakresu [0,1], bo random losuje [0,1), a uniform(a,b)-, nie daje gwarancji domknięcia przedziału z prawej strony
        liczba2 = (random.randint(0,10**9)*1.0 /10**9)

        if(pow(liczba1,2) + pow(liczba2,2)<=1 ):
            w_kole = w_kole + 1

        i = i + 1

    return float(4*w_kole)/n
try:

    print calc_pi(10)
    print calc_pi(1000)
    print calc_pi(1000000)
except ValueError:
    print "Pamiętaj by wpisać nastepnym razem liczbę losowań większą od 0. Powodzenia."

# Jak widać, im więcej losowań, tym większa dokładność. Dla n=1000 otrzymujemy już w miare dobrą dokładność ( do 1 miejsca po przecinku).
#Czyli teoretycznie wykonując nieskończenie wiele losowań możemy osiągnąć nieskończenie dobrą dokładność.
# Ale to teoria - w praktyce wybieramy takie n, żeby otrzymać interesującą nas dokładność.
