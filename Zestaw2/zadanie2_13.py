# -*- coding: iso-8859-2 -*-
#
#Znaleźć łączną długość wyrazów w napisie line. Wskazówka: można skorzystać z funkcji sum(). 

line = "ala ma kota \n kot ma ale\n  kto ma psa"

print sum( len(word) for word in line.split() )
