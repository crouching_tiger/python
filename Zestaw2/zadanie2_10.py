# -*- coding: iso-8859-2 -*-
#
#Mamy dany napis wielowierszowy line. Podać sposób obliczenia liczby wyrazów w napisie.
#Przez wyraz rozumiemy ciąg "czarnych" znaków, oddzielony od innych wyrazów białymi znakami (spacja, tabulacja, newline).

line = "ab\n cd\tef qw"
print len( line.split() )
