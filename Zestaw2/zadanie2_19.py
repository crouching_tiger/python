# -*- coding: iso-8859-2 -*-
#
#Na liście L mamy liczby jedno-, dwu- i trzycyfrowe dodatnie. 
#Chcemy zbudować napis z trzycyfrowych bloków, gdzie liczby jedno- i dwucyfrowe będą miały blok dopełniony zerami, np. 007, 024. 
#Wskazówka: str.zfill(). 

L = [1 , 22 , 33 , 44 , 555, 654, 4 , 1 , 6]

napis=""

for number in map(str,L):
	napis += number.zfill(3)

print napis
