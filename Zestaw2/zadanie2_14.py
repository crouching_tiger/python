# -*- coding: iso-8859-2 -*-
#
#Znaleźć: (a) najdłuższy wyraz, (b) długość najdłuższego wyrazu w napisie line.
 
line = "ala ma kota \n kot ma ale\n  kto ma psa \n programowanie w Pythonie"

print "Najdłuższy wyraz:"
print max( line.split(), key = len )

print "Długość najdłuższego wyrazu:"
print  max ( len(word) for word in line.split() )


