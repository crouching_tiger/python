# -*- coding: iso-8859-2 -*-
#
#Stworzyć plik fracs.py i zapisać w nim funkcje do działań na ułamkach.
#Ułamek będzie reprezentowany przez listę dwóch liczb całkowitych [licznik, mianownik].
#Napisać kod testujący moduł fracs. Nie należy korzystać z klasy Fraction z modułu fractions.
#Można wykorzystać funkcję fractions.gcd() implementującą algorytm Euklidesa. 
#
#f1 = [-1, 2]                  # -1/2
#f2 = [0, 1]                   # zero
#f3 = [3, 1]                   # 3
#f4 = [6, 2]                   # 3 (niejednoznaczność)
#f5 = [0, 2]                   # zero (niejednoznaczność)

from fractions import gcd

def add_frac(frac1, frac2):        # frac1 + frac2
    mianownik = frac1[1] * frac2[1] / gcd(frac1[1], frac2[1])
    frac = [ frac1[0] * mianownik / frac1[1] + frac2[0] * mianownik / frac2[1] , mianownik]
    divider = gcd(frac[0], frac[1])
    frac = [ frac[0] / divider, frac[1] / divider]
    return frac

def sub_frac(frac1, frac2):        # frac1 - frac2
    frac = list( frac2 )
    frac[0] = -frac[0]
    return add_frac( frac1, frac )

def mul_frac(frac1, frac2):        # frac1 * frac2
    frac = [ frac1[0] * frac2[0], frac1[1] * frac2[1] ]
    divider = gcd( frac[0], frac[1] )
    frac = [ frac[0] / divider, frac[1] / divider ]
    return frac

def div_frac(frac1, frac2):        # frac1 / frac2
    frac = [ frac2[1], frac2[0] ]
    return mul_frac( frac1, frac )

def is_positive(frac):             # bool, czy dodatni
    if (frac[0] * frac[1]) > 0:
        return True
    else:
        return False

def is_zero(frac):                 # bool, typu [0, x]
    if frac[0] == 0:
        return True
    else:
        return False

def cmp_frac(frac1, frac2):        # -1 | 0 | +1
    frac = sub_frac( frac1, frac2 )
    if is_zero( frac ):            # frac1 == frac2
        return 0
    elif is_positive( frac ):      # frac1 > frac2
        return 1
    else:                          # frac1 < frac2
        return -1

def frac2float(frac):              # konwersja do float
    return float( frac[0] ) / float( frac[1] )


