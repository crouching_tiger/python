# -*- coding: iso-8859-2 -*-
#

def factorial(n):
    if n == 0:
        return 1
    else:
        result = n
        while( n > 1):
            result *= n-1
            n = n - 1
        return result



def fibonacci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        fib = [ 0, 1]
        for i in xrange( 2, n+1):
            fib.append(fib[i-1] + fib[i-2])
    return fib[n]

