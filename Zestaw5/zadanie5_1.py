# -*- coding: iso-8859-2 -*-
#
#Stworzyć plik rekurencja.py i zapisać w nim funkcje z zadań 4.3 (factorial), 4.4 (fibonacci).
#Sprawdzić operacje importu i przeładowania modułu.
#
#import rekurencja
# import rekurencja as rek
# from rekurencja import *
# from rekurencja import factorial
# from rekurencja import fibonacci as fib
#
#print rekurencja.factorial(6)
#print rekurencja.fibonacci(5)



#1

import rekurencja
print rekurencja.factorial(6)
print rekurencja.fibonacci(5)

reload(rekurencja) # ponowne ładowanie modułu

del rekurencja
# import modułu tworzy przestrzeń nazw rekurencja
#zakres globalny pliku modułu staje się po zaimportowaniu przestrzenią nazw atrybutów obiektu modułu. 

#--------------------------------------------------------------------------------------------------------
#2
import rekurencja as rek
print rek.factorial(6)
print rek.fibonacci(5)

del rek
# import modułu rekurencja pod nazwą rek , równowaznie można by zaimportować rekurencja(import rekurencja),
# przypisać ją do nowego obiektu rek=rekurencja, i usunąć dowiązanie do obiektu rekurencja(del rekurencja)
# import modułu tworzy przestrzeń nazw rek

#---------------------------------------------------------------------------------------------------------
#3
from rekurencja import *
print factorial(6)
print fibonacci(5)

del factorial
del fibonacci
# ładowanie wszystkich nazw z modułu rekurencja
#Instrukcja from niszczy podział przestrzeni nazw, ponieważ nazwy są importowane bezpośrednio do lokalnej tablicy symboli.
# Sama nazwa modułu, z którego importowane są nazwy, nie jest ustawiana.

#-------------------------------------------------------------------------------------------------
#4
from rekurencja import factorial
print factorial(6)

#ładujemy z modułu rekurencja tylko funkcje fibonacci, do lokalnej tablicy symboli, nie ma osibnej przestrzeni nazw

#--------------------------------------------------------------------------------------------------------------
#5
from rekurencja import fibonacci as fib
print fib(5)

# ładujemy nazwę fibonacci z modułu rekurencja, i ustawiamy jej nazwę na fib( pod tą nazwą jest zapisywana w lokalnej tablicy symboli)

