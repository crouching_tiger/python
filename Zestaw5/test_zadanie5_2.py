# -*- coding: iso-8859-2 -*-
#
#zadanie5_2.py - testowanie
#

import unittest

from zadanie5_2 import *

class TestFractions(unittest.TestCase):

    def setUp(self):
        self.zero = [0, 1]

    def test_add_frac(self):
        self.assertEqual(add_frac([1, 2], [1, 3]), [5, 6])    # >0 , >0 l.niecałkowita
        self.assertEqual(add_frac([-1, 2], [1, 3]), [-1, 6])  # <0 , >0 l.niecałkowita
        self.assertEqual(add_frac([1, 2], [-1, 3]), [1, 6])   # >0 , <0 l.niecałkowita
        self.assertEqual(add_frac([-1, 2], [-1, 3]), [-5, 6]) # <0 , <0 l.niecałkowita
        self.assertEqual(add_frac([-1, 2], [1, 2]), [0, 1])   # <0 , >0 l.niecałkowita
        self.assertEqual(add_frac([1, 2], [-1, 2]), [0, 1])   # >0 , <0 l.niecałkowita

        self.assertEqual(add_frac([-11, 2], [111, 3]), [63, 2]) # <0 , >0 l.niecałkowita

        self.assertEqual(add_frac([1, 1], [1, 1]), [2, 1])    # >0 , >0 l.całkowita
        self.assertEqual(add_frac([-1, 1], [-1, 1]), [-2, 1]) # >0 , >0 l.całkowita
        self.assertEqual(add_frac([-1, 1], [1, 1]), [0, 1])   # >0 , >0 l.całkowita
        self.assertEqual(add_frac([1, 1], [-1, 1]), [0, 1])   # >0 , >0 l.całkowita

        self.assertEqual(add_frac([0, 1], [0, 1]), [0, 1]) # ==0 , ==0

    def test_sub_frac(self):
        self.assertEqual(sub_frac([1, 2], [1, 3]), [1, 6])    # >0 , >0 l.niecałkowita
        self.assertEqual(sub_frac([-1, 2], [1, 3]), [-5, 6])  # <0 , >0 l.niecałkowita
        self.assertEqual(sub_frac([1, 2], [-1, 3]), [5, 6])   # >0 , <0 l.niecałkowita
        self.assertEqual(sub_frac([-1, 2], [-1, 3]), [-1, 6]) # <0 , <0 l.niecałkowita
        self.assertEqual(sub_frac([-1, 2], [1, 2]), [-1, 1])  # <0 , >0 l.niecałkowita
        self.assertEqual(sub_frac([1, 2], [-1, 2]), [1, 1])   # >0 , <0 l.niecałkowita
        self.assertEqual(sub_frac([1, 2], [1, 2]), [0, 1])    # >0 , >0 l.niecałkowita

        self.assertEqual(sub_frac([1, 1], [1, 1]), [0, 1])   # >0 , >0 l.całkowita
        self.assertEqual(sub_frac([-1, 1], [-1, 1]), [0, 1]) # >0 , >0 l.całkowita
        self.assertEqual(sub_frac([-1, 1], [1, 1]), [-2, 1]) # >0 , >0 l.całkowita
        self.assertEqual(sub_frac([1, 1], [-1, 1]), [2, 1])  # >0 , >0 l.całkowita

        self.assertEqual(sub_frac([0, 1], [0, 1]), [0, 1]) # ==0 , ==0

    def test_mul_frac(self):
        self.assertEqual(mul_frac([1, 2], [1, 3]), [1, 6])   # >0 , >0 l.niecałkowita
        self.assertEqual(mul_frac([-1, 2], [1, 3]), [-1, 6]) # <0 , >0 l.niecałkowita
        self.assertEqual(mul_frac([1, 2], [-1, 3]), [-1, 6]) # >0 , <0 l.niecałkowita
        self.assertEqual(mul_frac([-1, 2], [-1, 3]), [1, 6]) # <0 , <0 l.niecałkowita
        self.assertEqual(mul_frac([-1, 2], [1, 2]), [-1, 4]) # >0 , >0 l.niecałkowita
        self.assertEqual(mul_frac([1, 2], [-1, 2]), [-1, 4]) # >0 , <0 l.niecałkowita
        self.assertEqual(mul_frac([1, 2], [1, 2]), [1, 4])   # >0 , >0 l.niecałkowita

        self.assertEqual(mul_frac([0, 2], [1, 2]), [0, 1]) # ==0 , >0 l.niecałkowita
        self.assertEqual(mul_frac([1, 2], [0, 2]), [0, 1]) # >0 , ==0 l.niecałkowita
        self.assertEqual(mul_frac([0, 2], [0, 2]), [0, 1]) # ==0 , ==0 l.niecałkowita

        self.assertEqual(mul_frac([1, 1], [1, 1]), [1, 1])   # >0 , >0 l.całkowita
        self.assertEqual(mul_frac([-1, 1], [-1, 1]), [1, 1]) # >0 , >0 l.całkowita
        self.assertEqual(mul_frac([-1, 1], [1, 1]), [-1, 1]) # >0 , >0 l.całkowita
        self.assertEqual(mul_frac([1, 1], [-1, 1]), [-1, 1])  # >0 , >0 l.całkowita

    def test_div_frac(self):
        self.assertEqual(div_frac([1, 2], [1, 3]), [3, 2])   # >0 , >0 l.niecałkowita
        self.assertEqual(div_frac([-1, 2], [1, 3]), [-3, 2]) # <0 , >0 l.niecałkowita
        self.assertEqual(div_frac([1, 2], [-1, 3]), [-3, 2]) # >0 , <0 l.niecałkowita
        self.assertEqual(div_frac([-1, 2], [-1, 3]), [3, 2]) # <0 , <0 l.niecałkowita
        self.assertEqual(div_frac([-1, 2], [1, 2]), [-1, 1]) # <0 , >0 l.niecałkowita
        self.assertEqual(div_frac([1, 2], [-1, 2]), [-1, 1]) # >0 , <0 l.niecałkowita
        self.assertEqual(div_frac([1, 2], [1, 2]), [1, 1])   # >0 , >0 l.niecałkowita

        self.assertEqual(div_frac([0, 2], [1, 2]), [0, 1])   # ==0, >0, l.niecałkowita

        self.assertEqual(div_frac([1, 1], [1, 1]), [1, 1])   # >0 , >0 l.całkowita
        self.assertEqual(div_frac([-1, 1], [-1, 1]), [1, 1]) # >0 , >0 l.całkowita
        self.assertEqual(div_frac([-1, 1], [1, 1]), [-1, 1]) # >0 , >0 l.całkowita
        self.assertEqual(div_frac([1, 1], [-1, 1]), [-1, 1])  # >0 , >0 l.całkowita

    def test_is_positive(self):
        self.assertTrue(is_positive([1, 2])) # > 0 l.niecałkowita
        self.assertTrue(is_positive([3, 2])) # > 0 l.niecałkowita
        self.assertTrue(is_positive([2, 1])) # > 0 l. całkowita

        self.assertFalse(is_positive([-1, 2])) # < 0 l.niecałkowita
        self.assertFalse(is_positive([-3, 2])) # < 0 l. niecałkowita
        self.assertFalse(is_positive([-2, 1]))  # < 0 liczba całkowita

        self.assertFalse(is_positive([0, 1]))  # == 0


    def test_is_zero(self):
        self.assertTrue(is_zero([0,1])) # == 0

        self.assertFalse(is_zero([1,1])) # > 0 l. całkowita
        self.assertFalse(is_zero([1,2])) # > 0 l. niecałkowita

        self.assertFalse(is_zero([-1,1])) # < 0 l. całkowita
        self.assertFalse(is_zero([-1,2])) # < 0 l. niecałkowita

    def test_cmp_frac(self):
        self.assertEqual(cmp_frac([1, 2], [1, 2]), 0) # >0 ==  >0, l. niecałkowita 
        self.assertEqual(cmp_frac([-1, 2], [-1, 2]), 0) # <0 ==  <0, l. niecałkowita 
        self.assertEqual(cmp_frac([-1, 2], [1, 2]), -1) # <0  <  >0, l. niecałkowita 
        self.assertEqual(cmp_frac([1, 2], [-1, 2]), 1) # >0  >  <0, l. niecałkowita 

        self.assertEqual(cmp_frac([0, 1], [0, 1]), 0) # 0 == 0 

        self.assertEqual(cmp_frac([-1, 1], [-1, 1]), 0) # <0  ==  <0, l. całkowita 
        self.assertEqual(cmp_frac([-1, 1], [1, 1]), -1) # <0  <  >0, l. całkowita 
        self.assertEqual(cmp_frac([1, 1], [-1, 1]), 1) # >0  >  <0, l. całkowita 
        self.assertEqual(cmp_frac([1, 1], [1, 1]), 0) # >0  ==  >0, l. całkowita 


    def test_frac2float(self):
        self.assertEqual(frac2float([1, 1]), 1.0) # >0 l. całkowita 
        self.assertEqual(frac2float([-1, 1]), -1.0) # <0 l. całkowita 
        self.assertEqual(frac2float([0, 1]), 0.0)  # == 0

        self.assertEqual(frac2float([1, 2]), 0.5)  # >0 l. niecałkowita 
        self.assertEqual(frac2float([1, 4]), 0.25) # >0 l. niecałkowita 
        self.assertEqual(frac2float([1, 10]), 0.1) # >0 l. niecałkowita 

        self.assertEqual(frac2float([-1, 2]), -0.5)  # <0 l. niecałkowita 
        self.assertEqual(frac2float([-1, 4]), -0.25)  # <0 l. niecałkowita 
        self.assertEqual(frac2float([-1, 10]), -0.1)  # <0 l. niecałkowita 


    def tearDown(self): pass

if __name__ == '__main__':
    unittest.main()     # uruchamia wszystkie testy

