# -*- coding: iso-8859-2 -*-
#
#Stworzyć implementację tablicową stosu do przechowywania bez powtórzeń liczb całkowitych od 0 do size-1.
# Powtarzająca się liczba ma być ignorowana bez żadnej akcji (inne podejście to wyzwolenie wyjątku).
# Wskazówka: stworzyć drugą tablicę, w której 0 lub 1 na pozycji i oznacza odpowiednio brak lub istnienie liczby i na stosie. 
class Stack:

    def __init__(self, size=10):
        """Initialize stack."""
        self.items = size * [None]      # utworzenie tablicy
        self.n = 0                      # liczba elementów na stosie
        self.size = size
        self.included = size * [False]    # tablica do sprawdzania powtórzeń

    def is_empty(self):
        """Check if stack is empty."""
        return self.n == 0

    def is_full(self):
        """Check if stack is full."""
        return self.size == self.n

    def push(self, data):
        """Add an element to stack."""
        if self.included[data]:    #if number is included - ignore push
            return
        self.items[self.n] = data
        self.included[ data] = True    #set included element as True
        self.n += 1

    def pop(self):
        """Take an element from stack."""
        self.n -= 1
        data = self.items[self.n]
        self.included[data] = False    # set included element on false
        self.items[self.n] = None    # usuwam referencję
        return data

    def printStack(self):
        """Print elements of stack and delete them."""
        while not self.is_empty():
            print self.pop()

stack = Stack()
stack.push(1)
stack.push(1)
stack.push(2)
stack.push(3)
stack.push(4)
stack.push(2)
stack.push(4)
stack.push(6)
stack.push(9)

stack.printStack()
