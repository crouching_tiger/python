# -*- coding: iso-8859-2 -*-
#
#Stworzyć ADT w postaci kolejki losowej, z której elementy będą pobierane w losowej kolejności.
# Zadbać o to, aby każda operacja była wykonywana w stałym czasie, niezależnie od liczby elementów w kolejce.

#Tutaj - złożonośc funkcji O(1), ale jeżeli założymy że random.randomint() jest O(1) - tego nie mogłem znaleźć.
#Ewentualnie jeśli nie, to można napisać własną funkcję podobną do random, która będzie O(1)

import random

class RandomQueue:    #za pomocą tablic

    def __init__(self, size=11):
        self.n = size + 1         # faktyczny rozmiar tablicy
        self.items = self.n * [None] 
        self.head = 0           # pierwszy do pobrania 
        self.tail = 0           # pierwsze wolne


    def insert(self, item):
        self.items[self.tail] = item
        self.tail = (self.tail + 1) % self.n

    def remove(self):   # zwraca losowy element
        a = self.losuj()
        if a == self.head:
            data = self.items[self.head]
            self.items[self.head] = None      # usuwam referencję
            self.head = (self.head + 1) % self.n
        else:
            data = self.items[a]
            self.items[a] = self.items[self.head]
            self.items[self.head] = None      # usuwam referencję
            self.head = (self.head + 1) % self.n

        return data

    def is_empty(self):
        return self.head == self.tail

    def is_full(self):
        return (self.head + self.n-1) % self.n == self.tail

    def printQueue(self):    #jednocześnie usuwa elementy z kolejki
        """Print elements of the queue and delete them."""
        while not self.is_empty():
            print self.remove()

    def losuj(self):    #losujemy sobie indeks elementu który weźmiemy z kolejki
        if self.head <= self.tail:
            return random.randint(self.head, self.tail-1)
        else:
            return random.randint(self.head, self.tail+ self.n ) % self.n

k1 = RandomQueue()

k1.insert(1)
k1.insert(11)
k1.insert(21)
k1.insert(141)
k1.insert(12)
k1.insert(16)
k1.insert(126)
k1.insert(3)
k1.insert(8)
k1.insert(6)

k1.printQueue()


