# -*- coding: iso-8859-2 -*-
#
#W pythonowej implementacji kolejki priorytetowej dodać możliwość ustawiania sposobu porównywania elementów znajdujących się w kolejce. 

#Tutaj za pomocą kolejki priorytetowej udało mi się zrobić kolejkę losową.
# Wystarczy jako funkcję sprawdzającą (porównującą) zapisać coś, co daje losowe wartości porównywania priorytetów.
#Ale nie uzyskujemy stałego czasu funkcji.

import random

class PriorityQueue:

    def __init__(self, cmpfunc=cmp):
        """Initialize Priority gueue."""
        self.items = []
        self.cmpfunc = cmpfunc

    def __str__(self):   # podglądamy kolejkę
        """Podglądamy kolejkę."""
        return str(self.items)

    def is_empty(self):
        """Check if queue is empty."""
        return not self.items

    def insert(self, item):
        """Insert an element to queue."""
        self.items.append(item)

    def remove(self):
        """Remove an element from queue."""
        maxi = 0
        for i in range(1, len(self.items)):
            if self.cmpfunc( self.items[i] ,  self.items[maxi]) > 0:
                maxi = i
        return self.items.pop(maxi)

    def printQueue(self):    #jednocześnie usuwa elementy z kolejki
        """Print elements of the queue and delete them."""
        while not self.is_empty():
            print self.remove()


def cmp1(x,y):
        return bool(random.getrandbits(1))



#sortowanie malejące

print "W losowej kolejności:"
k1 = PriorityQueue(cmp1)

k1.insert(1)
k1.insert(11)
k1.insert(21)
k1.insert(141)
k1.insert(12)
k1.insert(16)
k1.insert(126)
k1.insert(3)
k1.insert(8)
k1.insert(6)

k1.printQueue()


