# -*- coding: iso-8859-2 -*-
#
#Napisać iteracyjną wersję funkcji factorial(n) obliczającej silnię.
 
def factorial(n):
    if n == 0:
        return 1
    else:
        result = n
        while( n > 1):
            result *= n-1
            n = n - 1
        return result

print factorial(9)
