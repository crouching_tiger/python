# -*- coding: iso-8859-2 -*-
#
#Napisać program rysujący "miarkę" o zadanej długości.
#Należy prawidłowo obsłużyć liczby składające się z kilku cyfr.
#Należy zbudować pełny string, a potem go wypisać.
#
#|....|....|....|....|....|....|....|....|....|....|....|....|
#0    1    2    3    4    5    6    7    8    9   10   11   12
#
#Rozwiązania zadań 3.5 i 3.6 z poprzedniego zestawu zapisać w postaci funkcji, które zwracają pełny string przez return. 
import sys
import math
def rysuj(X):

    if not isinstance(X, int):
        print "Niepoprawna wartość na wejściu. Error."  
        sys.exit
    else:

        if X > 15:
            print "Miarka zbyt duża by zmieścić się na standardowym wymiarze konsoli"
     	    sys.exit

        if X < 0:
		    print "Miarka nie może być ujemna"
 		    sys.exit

        new_output = "|"

        for i in xrange(1, X+1):
            new_output +="....|"

        new_output +="\n0"

        for i in xrange(1, X+1):
       	    for j in xrange(4 - int(math.floor(math.log(i,10)))):
                new_output += " "
            new_output += str(i)

        return new_output

miarka=rysuj(3)
print miarka
