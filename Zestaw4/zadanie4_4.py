# -*- coding: iso-8859-2 -*-
#
#Napisać iteracyjną wersję funkcji fibonacci(n) obliczającej n-ty wyraz ciągu Fibonacciego. 

def fibonacci(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        fib = [ 0, 1]
        for i in xrange( 2, n+1):
            fib.append(fib[i-1] + fib[i-2])
    return fib[n]

print fibonacci(9)
