# -*- coding: iso-8859-2 -*-
#
#Napisać funkcję odwracanie(L, left, right) odwracającą kolejność elementów na liście od numeru left do right włącznie.
#Lista jest modyfikowana w miejscu (in place). Rozważyć wersję iteracyjną i rekurencyjną.

def odwracanie(L, left, right):
    if left > right:
        pom = left
        left = right
        right = pom

    srodek = (left + right)/ 2

    while( left <= srodek):
        pom = L[left]
        L[left] = L[right]
        L[right] = pom
        left = left + 1
        right = right - 1

def odwracanie1(L, left, right):

        srodek = (left + right) / 2

        if( left <= srodek):
            pom = L[left]
            L[left] = L[right]
            L[right] = pom
            left = left + 1
            right = right - 1
            return odwracanie1(L, left, right)

L=[1,2,3,4,5,6,7,8,9]
odwracanie ( L,0,8)
print L
odwracanie1 ( L,0,8)
print L
