# -*- coding: iso-8859-2 -*-
#
#Napisać program rysujący prostokąt zbudowany z małych kratek.
#Należy zbudować pełny string, a potem go wypisać. Przykładowy prostokąt składający się 2x4 pól ma postać:
#
#+---+---+---+---+
#|   |   |   |   |
#+---+---+---+---+
#|   |   |   |   | 
#+---+---+---+---+
#
#Rozwiązania zadań 3.5 i 3.6 z poprzedniego zestawu zapisać w postaci funkcji, które zwracają pełny string przez return. 
def rysuj(Y,X):

    if not ( isinstance(X, int) or isinstance(Y,int) ):
        print "Niepoprawna wartość na wejściu. Error."  
        sys.exit
    else:
        new_output = ""

        for i in xrange(Y):
            for j in xrange(X):
                new_output += "+---"
            new_output += "+\n"
            for j in xrange(X):
                new_output += "|   "
            new_output += "|\n"

        for j in xrange(X):
            new_output += "+---"
        new_output += "+\n"

    return new_output

print rysuj(2,4)
