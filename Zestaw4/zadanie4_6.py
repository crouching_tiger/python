# -*- coding: iso-8859-2 -*-
#
#Napisać funkcję sum_seq(sequence) obliczającą sumę liczb zawartych w sekwencji, która może zawierać zagnieżdżone podsekwencje.
#Wskazówka: rozważyć wersję rekurencyjną, a sprawdzanie, czy element jest sekwencją, wykonać przez isinstance(item, (list, tuple)).
#
# Wersja sum_seq1() jest szybsza, natomiast sum_seq() pozwala na ponowne wykorzystanie kodu z zadania numer 7.
# 
def flatten( sequence ):
    Lista = list()
    for i in xrange( len( sequence ) ):
        if isinstance( sequence[i], (list, tuple) ):
            Lista.extend( flatten( sequence[i] ) )
        else:
            Lista.append( sequence[i] )
    return Lista

def sum_seq(sequence):
    sequence = flatten(sequence)
    return sum( i for i in sequence)

def sum_seq1( sequence ):
    sum = 0
    for i in xrange( len( sequence ) ):
        if isinstance( sequence[i], (list, tuple) ):
            sum += sum_seq1( sequence[i] )
        else:
            sum += sequence[i]
    return sum

seq = [1,(2,3),[],[4,(5,6,7)],8,[9]]
print sum_seq1( seq )
print sum_seq( seq )


