# -*- coding: iso-8859-2 -*-
#
#zadanie7_1
#W pliku fracs.py zdefiniować klasę Frac wraz z potrzebnymi metodami.
#Wykorzystać wyjątek ValueError do obsługi błędów w ułamkach.
#Dodać możliwości dodawania liczb (int, long) do ułamków (działania lewostronne i prawostronne).
#Rozważyć możliwość włączenia liczb float do działań na ułamkach [Wskazówka: metoda float.as_integer_ratio()].
#Napisać kod testujący moduł fracs.

from fractions import gcd


class Frac:
    """Klasa reprezentująca ułamki."""

    def __init__(self, x=0, y=1):
       
        if x%1 ==0 and y%1 ==0:   #x i y to liczby całkowite, format wejściowy: int x, int y
            if y == 0:            # Sprawdzamy, czy y=0.
                raise ValueError("mianownik ulamka nie moze byc rowny zero")
            self.x = int(x)
            self.y = int(y)

        elif isinstance(x, float):    # format wejściowy: liczba rzeczywista: float x
            args = x.as_integer_ratio()
            self.x = int(args[0])
            self.y = int(args[1])

        elif isinstance(x, int):    # format wejściowy: liczba całkowita: int x
            self.x = x
            self.y = 1
        else:
            raise ValueError("niepoprawny typ argumentów")

    def __str__(self):         # zwraca "x/y" lub "x" dla y=1
        """wypisz "x/y" lub "x" dla y=1"""
        if self.y == 1:
            return "%s" % self.x
        else:
            return "%s/%s" % (self.x, self.y)

    def __repr__(self):        # zwraca "Frac(x, y)"
        """Wypisz "Frac(x,y)" """
        return "Frac(%s, %s)" % (self.x, self.y)

    def __cmp__(self, other):  # porównywanie
        """Porównaj dwa ułamki"""
        other = self.toFrac(other)
        different = self-other
        if different.x == 0: # self == other
            return 0
        elif different.x*different.y > 0: # self > other
            return 1
        else:           # self < other
            return -1   

    def __add__(self, other):  # frac1+frac2, frac+int
        """Dodaj dwa ułamki lub ułamek i int"""
        other = self.toFrac(other)
        mianownik = int(self.y * other.y)
        frac = [ int( self.x * mianownik / self.y + other.x * mianownik / other.y ) , mianownik]
        divider = gcd(frac[0], frac[1])
        return Frac( frac[0] / divider, frac[1] / divider )

    __radd__ = __add__              # int+frac

    def __sub__(self, other):  # frac1-frac2, frac-int
        """Odejmij dwa ułamki lub ułamek i int."""
        other = self.toFrac( other )
        return self+(-other)

    def __rsub__(self, other):      # int-frac
        # tutaj self jest frac, a other jest int!
        return Frac(self.y * other - self.x, self.y)

    def __mul__(self, other):  # frac1*frac2, frac*int
        """Pomnóż dwa ułamki lub ułamek i int"""
        other = self.toFrac( other )
        frac = [ self.x * other.x, self.y * other.y ]
        divider = gcd( frac[0], frac[1] )
        return Frac( frac[0] / divider, frac[1] / divider )

    __rmul__ = __mul__              # int*frac

    def __div__(self, other):  # frac1/frac2, frac/int
        """Podziel dwa ułamki lub ułamek przez liczbę"""
        other = self.toFrac( other )
        return self * ~ other

    def __rdiv__(self, other): # int/frac
        """Podziel liczbę int przez ułamek"""
        other = self.toFrac( other )
        return other / self

    # operatory jednoargumentowe
    def __pos__(self):  # +frac = (+1)*frac
        """(+1)*Frac"""
        return self

    def __neg__(self):         # -frac = (-1)*frac
        """(+1)*Frac"""
        return Frac( -self.x, self.y)

    def __invert__(self):      # odwrotnosc: ~frac
        """Odwrotność ułamka"""
        return Frac( self.y, self.x )

    def __float__(self):       # float(frac)
        """Konwersja ułamka do postaci rzeczywistej"""
        return float( self.x ) / float ( self.y )

    def toFrac(self,x):
        if isinstance(x, Frac):
            return x
        elif isinstance(x, float) or isinstance(x, int):
            return Frac(x)
        else:
            raise ValueError("niepoprawny typ argumentów")

#print Frac(0,1)/Frac(0,1)
