# -*- coding: iso-8859-2 -*-
#
#testowanie - zadanie7_1
# Kod testujący moduł.

import unittest
from fracs import *

class TestFrac(unittest.TestCase):

    def setUp(self): pass

    def test_str(self):
        self.assertEqual( Frac.__str__( Frac(5,6) ), "5/6" )
        self.assertEqual( Frac.__str__( Frac(0,1) ), "0" )
        self.assertEqual( Frac.__str__( Frac(-1,-1) ), "-1/-1" )
        self.assertEqual( Frac.__str__( Frac(-5,6) ), "-5/6" )
        self.assertEqual( Frac.__str__( Frac(5,-6) ), "5/-6" )

        self.assertEqual( Frac.__str__( Frac(5) ), "5" )
        self.assertEqual( Frac.__str__( Frac(-6) ), "-6" )
        self.assertEqual( Frac.__str__( Frac(0) ), "0" )

        self.assertEqual( Frac.__str__( Frac(-0.5) ), "-1/2" )
        self.assertEqual( Frac.__str__( Frac(-0.25) ), "-1/4" )
        self.assertEqual( Frac.__str__( Frac(6.5) ), "13/2" )

    def test_repr(self):
        self.assertEqual( Frac.__repr__( Frac(5,6) ), "Frac(5, 6)" )
        self.assertEqual( Frac.__repr__( Frac(0,1) ), "Frac(0, 1)" )
        self.assertEqual( Frac.__repr__( Frac(-1,-1) ), "Frac(-1, -1)" )
        self.assertEqual( Frac.__repr__( Frac(-5,6) ), "Frac(-5, 6)" )
        self.assertEqual( Frac.__repr__( Frac(5,-6) ), "Frac(5, -6)" )

        self.assertEqual( Frac.__repr__( Frac(5) ), "Frac(5, 1)" )
        self.assertEqual( Frac.__repr__( Frac(-6) ), "Frac(-6, 1)" )
        self.assertEqual( Frac.__repr__( Frac(0) ), "Frac(0, 1)" )

        self.assertEqual( Frac.__repr__( Frac(-0.5) ), "Frac(-1, 2)" )
        self.assertEqual( Frac.__repr__( Frac(-0.25) ), "Frac(-1, 4)" )
        self.assertEqual( Frac.__repr__( Frac(6.5) ), "Frac(13, 2)" )

    def test_cmp(self):
        self.assertEqual( Frac.__cmp__( Frac(0,1), Frac(0,1) ), 0 )
        self.assertEqual( Frac.__cmp__( Frac(1,1), Frac(1,1) ), 0 )
        self.assertEqual( Frac.__cmp__( Frac(-1,1), Frac(-1,1) ), 0 )

        self.assertEqual( Frac.__cmp__( Frac(1,1), Frac(0,1) ), 1 )
        self.assertEqual( Frac.__cmp__( Frac(0,1), Frac(1,1) ), -1 )

        self.assertEqual( Frac.__cmp__( Frac(2.5), Frac(2.5) ), 0 )
        self.assertEqual( Frac.__cmp__( Frac(2.5), Frac(1.5) ), 1 )
        self.assertEqual( Frac.__cmp__( Frac(1.5), Frac(2.5) ), -1 )
        self.assertEqual( Frac.__cmp__( Frac(2.5), Frac(-2.5) ), 1 )
        self.assertEqual( Frac.__cmp__( Frac(-2.5), Frac(2.5) ), -1 )
        self.assertEqual( Frac.__cmp__( Frac(-2.5), Frac(-2.5) ), 0 )

        self.assertEqual( Frac.__cmp__( Frac(2), Frac(2) ), 0 )
        self.assertEqual( Frac.__cmp__( Frac(-1), Frac(-1) ), 0 )
        self.assertEqual( Frac.__cmp__( Frac(2), Frac(1) ), 1 )
        self.assertEqual( Frac.__cmp__( Frac(1), Frac(2) ), -1 )

    def test_add(self):
        self.assertEqual(Frac(1, 2) + Frac(1, 3), Frac(5, 6) )    # >0 , >0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) + Frac(1, 3), Frac(-1, 6) )  # <0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) + Frac(-1, 3), Frac(1, 6) )   # >0 , <0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) + Frac(-1, 3), Frac(-5, 6) ) # <0 , <0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) + Frac(1, 2), Frac(0, 1) )   # <0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) + Frac(-1, 2), Frac(0, 1) )   # >0 , <0 l.niecałkowita

        self.assertEqual(Frac(-11, 2) + Frac(111, 3), Frac(63, 2) ) # <0 , >0 l.niecałkowita

        self.assertEqual(Frac(1, 1) + Frac(1, 1), Frac(2, 1) )    # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1, 1) + Frac(-1, 1), Frac(-2, 1) ) # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1, 1) + Frac(1, 1), Frac(0, 1) )   # >0 , >0 l.całkowita
        self.assertEqual(Frac(1, 1) + Frac(-1, 1), Frac(0, 1) )   # >0 , >0 l.całkowita

        self.assertEqual(Frac(0, 1) + Frac(0, 1), Frac(0, 1) ) # ==0 , ==0

	self.assertEqual( Frac(1.5) + Frac(1) , Frac(5,2) )
	self.assertEqual( Frac(-1.5) + Frac(1) , Frac(-1,2) )

	self.assertEqual( Frac(1,1) + 1 , Frac(2,1) )
	self.assertEqual( Frac(-1,1) + 1 , Frac(0,1) )

	self.assertEqual( Frac(1) + 1 , Frac(2,1) )
	self.assertEqual( Frac(1.5) + 1 , Frac(5,2) )

    def test_radd(self):
	self.assertEqual( 1 + Frac(1,1), Frac(2,1) )
	self.assertEqual( 1 + Frac(-1,1), Frac(0,1) )

	self.assertEqual( 1 + Frac(1), Frac(2,1) )
	self.assertEqual( 1 + Frac(1.5), Frac(5,2) )

    def test_sub(self):
        self.assertEqual(Frac(1, 2) - Frac(1, 3), Frac(1, 6) )    # >0 , >0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) - Frac(1, 3), Frac(-5, 6) )  # <0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) - Frac(-1, 3), Frac(5, 6) )   # >0 , <0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) - Frac(-1, 3), Frac(-1, 6) ) # <0 , <0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) - Frac(1, 2), Frac(-1, 1) )  # <0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) - Frac(-1, 2), Frac(1, 1) )   # >0 , <0 l.niecałkowita
        self.assertEqual(Frac(1, 2) - Frac(1, 2), Frac(0, 1) )    # >0 , >0 l.niecałkowita

        self.assertEqual(Frac(1, 1) - Frac(1, 1), Frac(0, 1) )   # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1, 1) - Frac(-1, 1), Frac(0, 1) ) # >0 , <0 l.całkowita
        self.assertEqual(Frac(-1, 1) - Frac(1, 1), Frac(-2, 1) ) # >0 , >0 l.całkowita
        self.assertEqual(Frac(1, 1) -Frac(-1, 1), Frac(2, 1) )   # >0 , <0 l.całkowita

        self.assertEqual(Frac(0, 1) - Frac(0, 1), Frac(0, 1) ) # ==0 , ==0

        self.assertEqual(Frac(1, 1) - 1, Frac(0, 1) )   # >0 , >0 l.całkowita

        self.assertEqual(Frac(1, 1) - 1, Frac(0, 1) )   # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1, 1) - (-1), Frac(0, 1) ) # >0 , <0 l.całkowita
        self.assertEqual(Frac(-1, 1) - 1, Frac(-2, 1) ) # >0 , >0 l.całkowita
        self.assertEqual(Frac(1, 1) - (-1), Frac(2, 1) )   # >0 , <0 l.całkowita

        self.assertEqual(Frac(0, 1) - 0, Frac(0, 1) ) # ==0 , ==0

        self.assertEqual(Frac(1) - 1, Frac(0, 1) )   # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1) - (-1), Frac(0, 1) ) # >0 , <0 l.całkowita
        self.assertEqual(Frac(-1) - 1, Frac(-2, 1) ) # >0 , >0 l.całkowita
        self.assertEqual(Frac(1) - (-1), Frac(2, 1) )   # >0 , <0 l.całkowita

        self.assertEqual( Frac(1.5) - 1, Frac(0.5) )

    def test_rsub(self):
        self.assertEqual( 1 - Frac(1,1) , Frac(0,1) )
        self.assertEqual( 1 - Frac(-1,1) , Frac(2,1) )
        self.assertEqual( 1 - Frac(0,1) , Frac(1,1) )
        self.assertEqual( 0 - Frac(1,1) , Frac(-1,1) )
        self.assertEqual( -1 - Frac(1,1) , Frac(-2,1) )

        self.assertEqual( 0 - Frac(0,1) , Frac(0,1) )

        self.assertEqual( 1 - Frac(1) , Frac(0,1) )
        self.assertEqual( 1 - Frac(1) , Frac(0,1) )

        self.assertEqual( 1 - Frac(0.5) , Frac(1,2) )
        self.assertEqual( 3 - Frac(-1.0) , Frac(4,1) )

    def test_mul(self):
        self.assertEqual(Frac(1, 2) * Frac(1, 3), Frac(1, 6) )   # >0 , >0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) * Frac(1, 3), Frac(-1, 6) ) # <0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) * Frac(-1, 3), Frac(-1, 6) ) # >0 , <0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) * Frac(-1, 3), Frac(1, 6) ) # <0 , <0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) * Frac(1, 2), Frac(-1, 4) ) # >0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) * Frac(-1, 2), Frac(-1, 4) ) # >0 , <0 l.niecałkowita
        self.assertEqual(Frac(1, 2) * Frac(1, 2), Frac(1, 4) )   # >0 , >0 l.niecałkowita

        self.assertEqual(Frac(0, 2) * Frac(1, 2), Frac(0, 1) ) # ==0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) * Frac(0, 2), Frac(0, 1) ) # >0 , ==0 l.niecałkowita
        self.assertEqual(Frac(0, 2) * Frac(0, 2), Frac(0, 1) ) # ==0 , ==0 l.niecałkowita

        self.assertEqual(Frac(1, 1) * Frac(1, 1), Frac(1, 1) )    # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1, 1) * Frac(-1, 1), Frac(1, 1) )  # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1, 1) * Frac(1, 1), Frac(-1, 1) )  # >0 , >0 l.całkowita
        self.assertEqual(Frac(1, 1) * Frac(-1, 1), Frac(-1, 1) )  # >0 , >0 l.całkowita

        self.assertEqual( Frac(1,1) * 1, Frac(1,1) )
        self.assertEqual( Frac(1,1) * 0, Frac(0,1) )
        self.assertEqual( Frac(1,1) * 3, Frac(3,1) )
        self.assertEqual( Frac(1,1) * -1, Frac(-1,1) )

        self.assertEqual( Frac(3) * 3, Frac(9) )
        self.assertEqual( Frac(0) * 0, Frac(0,1) )
        self.assertEqual( Frac(11) * 3, Frac(33,1) )

    def test_rmul(self):
        self.assertEqual( 1 * Frac(1,1), Frac(1,1) )
        self.assertEqual( 0 * Frac(1,1), Frac(0,1) )
        self.assertEqual( 3 * Frac(1,1), Frac(3,1) )
        self.assertEqual( -1 * Frac(1,1), Frac(-1,1) )

        self.assertEqual( 3 * Frac(3,1), Frac(9,1) )
        self.assertEqual( 0 * Frac(0,1), Frac(0,1) )
        self.assertEqual( 3 * Frac(11,1), Frac(33,1) )

    def test_div(self):
        self.assertEqual(Frac(1, 2) / Frac(1, 3), Frac(3, 2) )   # >0 , >0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) / Frac(1, 3), Frac(-3, 2) ) # <0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) / Frac(-1, 3), Frac(-3, 2) ) # >0 , <0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) / Frac(-1, 3), Frac(3, 2) ) # <0 , <0 l.niecałkowita
        self.assertEqual(Frac(-1, 2) / Frac(1, 2), Frac(-1, 1) ) # <0 , >0 l.niecałkowita
        self.assertEqual(Frac(1, 2) / Frac(-1, 2), Frac(-1, 1) ) # >0 , <0 l.niecałkowita
        self.assertEqual(Frac(1, 2)/ Frac(1, 2), Frac(1, 1) )    # >0 , >0 l.niecałkowita

        self.assertEqual(Frac(0, 2) / Frac(1, 2), Frac(0, 1) )   # ==0, >0, l.niecałkowita

        self.assertEqual(Frac(1, 1) / Frac(1, 1), Frac(1, 1) )    # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1, 1) / Frac(-1, 1), Frac(1, 1) )  # >0 , >0 l.całkowita
        self.assertEqual(Frac(-1, 1) / Frac(1, 1), Frac(-1, 1) )  # >0 , >0 l.całkowita
        self.assertEqual(Frac(1, 1) / Frac(-1, 1), Frac(-1, 1) )  # >0 , >0 l.całkowita

        self.assertEqual( Frac(3) / 1 , Frac(3,1) )
        self.assertEqual( Frac(3) / -1 , Frac(-3,1) )
        self.assertEqual( Frac(3) / 3 , Frac(1,1) )
        self.assertEqual( Frac(3) / 1 , Frac(3,1) )

        self.assertEqual( Frac(3) / Frac(1,3) , Frac(9,1) )

        self.assertEqual( Frac(2.5) / 1 , Frac(5,2) )

        self.assertRaises( ValueError, Frac.__div__, Frac(1,1), Frac(0,1) ) 
        self.assertRaises( ValueError, Frac.__div__, Frac(0,1), Frac(0,1) ) 
        self.assertRaises( ValueError, Frac.__div__, Frac(0,1), 0 ) 


    def test_rdiv(self):
        self.assertEqual( 1 / Frac(1,1), Frac(1,1) )
        self.assertEqual( 1 / Frac(2,1), Frac(1,2) )
        self.assertEqual( -1 / Frac(1,1), Frac(-1,1) )
        self.assertEqual( 0 / Frac(1,1), Frac(0,1) )
        self.assertEqual( 3 / Frac(1,1), Frac(3,1) )

        self.assertRaises( ValueError, Frac.__rdiv__, Frac(0,1), 1 ) 
        self.assertRaises( ValueError, Frac.__rdiv__, Frac(0), 1 ) 

    def test_pos(self):
        self.assertEqual( +Frac(0,1) , Frac(0,1) ) # +0 == 0
        self.assertEqual( +Frac(0) , Frac(0) ) # +0 == 0
        self.assertEqual( +Frac(0) , Frac(0,1) ) # +0 == 0
        self.assertEqual( +Frac(1,1) , Frac(1,1) ) # 1 
        self.assertEqual( +Frac(-1,1) , Frac(-1,1) ) # -1
        self.assertEqual( +Frac(1.25) , Frac(5,4) ) # +0 == 0

    def test_neq(self):
        self.assertEqual( -Frac(0,1) , Frac(0,1) ) # -0 == 0
        self.assertEqual( -Frac(1,1) , Frac(-1,1) ) # -1 
        self.assertEqual( -Frac(-1,1) , Frac(1,1) ) # 1

        self.assertEqual( -Frac(0) , Frac(0,1) ) # -0 == 0
        self.assertEqual( -Frac(1.5) , Frac(-3,2) ) # -1 
        self.assertEqual( -Frac(-3,1) , Frac(3,1) ) # 1

    def test_invert(self):
        self.assertEqual( ~Frac(1,1), Frac(1,1) ) 
        self.assertEqual( ~Frac(1,3), Frac(3,1) ) 
        self.assertEqual( ~Frac(-1,3), Frac(-3,1) ) 
        self.assertEqual( ~Frac(1,6), Frac(6,1) )
        self.assertEqual( ~Frac(1), Frac(1) ) 
        self.assertEqual( ~Frac(1.5), Frac(2, 3) ) 

        self.assertRaises( ValueError, Frac.__invert__, Frac(0,1) ) 
        self.assertRaises( ValueError, Frac.__invert__, Frac(0) ) 

    def test_float(self):
        self.assertEqual(Frac.__float__( Frac(1, 1) ), 1.0) # >0 l. całkowita 
        self.assertEqual(Frac.__float__( Frac(-1, 1) ), -1.0) # <0 l. całkowita 
        self.assertEqual(Frac.__float__( Frac(0, 1) ), 0.0)  # == 0

        self.assertEqual(Frac.__float__(Frac(1, 2) ), 0.5)  # >0 l. niecałkowita 
        self.assertEqual(Frac.__float__(Frac(1, 4) ), 0.25) # >0 l. niecałkowita 
        self.assertEqual(Frac.__float__(Frac(1, 10) ), 0.1) # >0 l. niecałkowita 

        self.assertEqual(Frac.__float__(Frac(-1, 2) ), -0.5)  # <0 l. niecałkowita 
        self.assertEqual(Frac.__float__(Frac(-1, 4) ), -0.25)  # <0 l. niecałkowita 
        self.assertEqual(Frac.__float__(Frac(-1, 10) ), -0.1)  # <0 l. niecałkowita 

        self.assertEqual(Frac.__float__(Frac(1.2) ), 1.2)  # >0 l. niecałkowita 

    def tearDown(self): pass

if __name__ == '__main__':
    unittest.main()     # uruchamia wszystkie testy

