# -*- coding: iso-8859-2 -*-
#
#W pliku circles.py zdefiniować klasę Circle wraz z potrzebnymi metodami.
#Okrąg jest określony przez podanie środka i promienia.
#Wykorzystać wyjątek ValueError do obsługi błędów.
#Napisać kod testujący moduł circles.

from points import Point
from math import pi

class Circle:
    """Klasa reprezentująca okręgi na płaszczyźnie."""

    def __init__(self, x=0, y=0, radius=1):
        if not ( ( isinstance(x, float) or isinstance(x, int) ) and ( isinstance(y, float) or isinstance(y, int) ) and ( isinstance(radius, float) or isinstance(radius, int) ) ):
            raise ValueError("niepoprawny typ argumentu")
        if radius < 0:
            raise ValueError("promień okręgu nie może być ujemny")
        self.centre = Point(x, y)
        self.radius = float(radius)

    def __repr__(self):       # "Circle(x, y, radius)"
        """Wświetlenie danych o okręgu w formie: "Circle(x, y, radius)." """
        return "Circle(%s, %s, %s)" % (self.centre.x, self.centre.y, self.radius)

    def __eq__(self, other):
        """Równość dwóch okręgów."""
        if not isinstance( other, Circle ):
            raise ValueError("niepoprawny typ argumentu")
        return self.centre == other.centre and self.radius == other.radius

    def __ne__(self, other):
        """Różność dwóch okręgów."""
        return not self == other

    def area(self):           # pole powierzchni
        """Pole powierzchni."""
        return pi * self.radius * self.radius

    def move(self, x, y):     # przesuniecie o (x, y)
        """Przesunięcie okręgu."""
        if not (isinstance( x, float ) or isinstance( x, int ) ) and (isinstance( y, float ) or isinstance( y, int ) ):
            raise ValueError("niepoprawny typ argumentu")
        return Circle( self.centre.x + x, self.centre.y + y, self.radius )


    def cover(self, other):   # okrąg pokrywający oba
        """Znajdowanie okręgu pokrywającego oba okręgi."""
        if not isinstance(other, Circle):
            raise ValueError("niepoprawny typ argumentu")
        elif other == self:
            return Circle(self.centre.x, self.centre.y, self.radius)
        else:
            R = max((self.radius + other.radius + ( other.centre - self.centre ).length() ) / 2, self.radius, other.radius )
            temp = ( R - self.radius ) / ( 2*R - self.radius - other.radius )
            Y = self.centre.y + temp * ( other.centre.y - self.centre.y )
            X = self.centre.x + temp * ( other.centre.x - self.centre.x )
        return Circle(X, Y, R)
