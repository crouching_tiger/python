# -*- coding: iso-8859-2 -*-
#
#testowanie klasy Circle

import unittest
from circles import *
from points import *

class TestCircle(unittest.TestCase):
    def setUp(self): pass

    def test_repr(self):
        self.assertEqual( Circle.__repr__( Circle() ), "Circle(0.0, 0.0, 1.0)" )
        self.assertEqual( Circle.__repr__( Circle(1,1,3) ), "Circle(1.0, 1.0, 3.0)" )
        self.assertEqual( Circle.__repr__( Circle(2.5,3.5,3.5) ), "Circle(2.5, 3.5, 3.5)" )
        self.assertEqual( Circle.__repr__( Circle(-2.5,3.5,3.5) ), "Circle(-2.5, 3.5, 3.5)" )
        self.assertEqual( Circle.__repr__( Circle(2.5,-3.5,3.5) ), "Circle(2.5, -3.5, 3.5)" )

    def test_eq(self):
        self.assertTrue( Circle(1,1,1) == Circle(1,1,1) )
        self.assertTrue( Circle(1.0,1.0,1.0) == Circle(1,1,1) )
        self.assertTrue( Circle(1.2,1,1) == Circle(1.2,1,1) )
        self.assertTrue( Circle(1,1.2,1) == Circle(1,1.2,1) )
        self.assertTrue( Circle(1.6,1.6,6) == Circle(1.6,1.6,6) )
        self.assertTrue( Circle(-1.6,1.6,6) == Circle(-1.6,1.6,6) )
        self.assertTrue( Circle(1.6,-1.6,6) == Circle(1.6,-1.6,6) )
        self.assertTrue( Circle(-1.6,1.6,6) == Circle(-1.6,1.6,6) )
        self.assertTrue( Circle() == Circle() )

        self.assertRaises( ValueError, Circle.__eq__, Circle(1,1,1), 1 )
        self.assertRaises( ValueError, Circle.__eq__, Circle(1,1,1), 1.3 )
        self.assertRaises( ValueError, Circle.__eq__, Circle(1,1,1), "1" )

        self.assertFalse( Circle(-1,1,1) == Circle(1,1,1) )
        self.assertFalse( Circle(1.0,1.0,1.0) == Circle(1,-1,1) )
        self.assertFalse( Circle(-1.2,1,1) == Circle(1.2,1,1) )

    def test_ne(self):
        self.assertTrue( Circle(1,1,1) != Circle(1,2,2) )
        self.assertTrue( Circle(1.1,1.1,1) != Circle(1,2,2) )
        self.assertTrue( Circle(1,1,1) != Circle(1,2.3,2.3) )
        self.assertTrue( Circle(-1,1,1) != Circle(1,-2,2) )

        self.assertRaises( ValueError, Circle.__ne__, Circle(1,1,1), 1 )
        self.assertRaises( ValueError, Circle.__ne__, Circle(1,1,1), 1.3 )
        self.assertRaises( ValueError, Circle.__ne__, Circle(1,1,1), "1" )

        self.assertFalse( Circle(1,1,1) != Circle(1,1,1) )
        self.assertFalse( Circle(1.0,1.0,1.0) != Circle(1,1,1) )
        self.assertFalse( Circle(1.2,1,1) != Circle(1.2,1,1) )

    def test_area(self):
        self.assertEqual( Circle.area( Circle() ), 1 * pi )
        self.assertEqual( Circle.area( Circle(1,1,1) ), 1 * pi )
        self.assertEqual( Circle.area( Circle(2,2,3) ), 9 * pi )
        self.assertEqual( Circle.area( Circle(1.2,1.2,3) ), 9 * pi )
        self.assertEqual( Circle.area( Circle(-1,-1,1) ), 1 * pi )
        self.assertEqual( Circle.area( Circle(1.0,1.0,1.5) ), 2.25 * pi )

    def test_move(self):
        self.assertEqual( Circle.move( Circle(), 1, 2 ), Circle(1, 2, 1) )
        self.assertEqual( Circle.move( Circle(), 1, 2.5 ), Circle(1, 2.5, 1) )
        self.assertEqual( Circle.move( Circle(1, 2, 2), 1, 2 ), Circle(2, 4, 2) )
        self.assertEqual( Circle.move( Circle(-1, -2, 2), 1, 2 ), Circle(0, 0, 2) )
        self.assertEqual( Circle.move( Circle(1, 2, 2), -1, -2 ), Circle(0, 0, 2) )
        self.assertEqual( Circle.move( Circle(11, 2.3, 2.4), 1, 2 ), Circle(12, 4.3, 2.4) )

        self.assertEqual( Circle.move( Circle(), 1, 2 ), Circle(1, 2, 1) )

        self.assertRaises( ValueError, Circle.move, Circle(1,1,1), Circle(1,1,1), 1 )
        self.assertRaises( ValueError, Circle.move, Circle(1,1,1), "a", 1 )

    def test_cover(self):    #zakładamy że znajdujemy minimalne koło pokrywające
        self.assertEqual( Circle.cover( Circle(1, 1, 1), Circle(2, 1, 1) ), Circle(1.5, 1, 1.5) )
        self.assertEqual( Circle.cover( Circle(1, 1, 8), Circle(2, 1, 1) ), Circle(1, 1, 8) )
        self.assertEqual( Circle.cover( Circle(1.6, -1.1, 1.6), Circle(2.0, 1.0, 22) ), Circle(2.0, 1.0, 22.0) )
        self.assertEqual( Circle.cover( Circle(-11, 1, 1), Circle(-11, 1, 1) ), Circle(-11, 1, 1) )
        self.assertEqual( Circle.cover( Circle(1, 1, 1), Circle(1, 1, 1) ), Circle(1, 1, 1) )
        self.assertEqual( Circle.cover( Circle(1, 1, 2), Circle(2, 1, 1) ), Circle(1, 1, 2) )
        self.assertEqual( Circle.cover( Circle(1, 1, 1), Circle(1,10, 3) ), Circle(1,6.5,6.5) )
        self.assertEqual( Circle.cover( Circle(1, 1, 1), Circle(22, 1, 11) ), Circle(16.5, 1, 16.5) )
        self.assertEqual( Circle.cover( Circle(1, 1, 1), Circle(1, 1.5, 1) ), Circle(1, 1.25, 1.25) )

    def tearDown(self): pass

if __name__ == '__main__':
    unittest.main()     # uruchamia wszystkie testy

